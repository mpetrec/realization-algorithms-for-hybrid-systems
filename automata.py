from sets import Set
import ConfigParser
from abstract_hankel_generate import *
from word_operations import *
from numpy import *


def ListToSet( list ):
	return_set = Set()
	list_of_visited = []
	element_count = 0
	for element in list:
		if not element in list_of_visited:
			return_set.add(element_count)
			list_of_visited.append(element)
			element_count = element_count +1
	
	return (return_set, list_of_visited)
		

class HankelTable:
	attribute_list = ["alphabet", "index_set", "table", "depth"]

	def __init__(self, **arguments):
		input_data = arguments

		for field in HankelTable.attribute_list:
			self.__dict__[field] = \
			   input_data[field]
	
	def __eq__ (self, other):
		return ( \
		  (self.alphabet == other.alphabet) and \
		  (self.index_set == other.index_set) and \
		  (self.table == other.table ) and \
		  (self.depth == other.depth ) )
	
	def ownprint(self):
		for field in HankelTable.attribute_list:
			print "Field: "+str(field)+"\n"
			print str(self.__dict__[field])+"\n"

	def __eq__(self, other):
		for field in HankelTable.attribute_list:
			if not self.__dict__[field] == \
			       other.__dict__[field]:
			       return False
		return True
	
	def ComputeShiftedColumn(self,start_index,\
	                            column, ocolumn, size):
		if size == 0:
			column.append(ocolumn[start_index+1])
		else:
			for nletter in range(len(self.alphabet)):
				nst_index = (start_index + 1)*\
				       len(self.alphabet)+\
				       nletter
				self.ComputeShiftedColumn(\
				   nst_index, column, ocolumn, \
				   size - 1)
	
		return
		
	def ShiftColumn(self,column, nletter):
		shifted_column = []
		for snum in range(self.depth+1):
			self.ComputeShiftedColumn(nletter, \
		   		shifted_column, column, snum)

		return shifted_column
	
	def FindIndex(self, column, state_list):
		length = len(column)
		for col_ind in range(len(state_list)):
			short_column = state_list[\
			                     col_ind][0:length]
			is_equal = True
			for sindex in range(length):
				if not short_column[sindex] ==\
				        column[sindex]:
					is_equal = False
					
					print \
			       "column[index]"+str(column[sindex])+\
			       " short_column[index] "+\
			       str(short_column[sindex])+\
			       " index "+str(sindex)+\
			      " is_equal: "+str(is_equal)+"\n"
					break
					
			print "Column\n"+str(column)+\
			       "short_column:\n"+str(short_column)+\
			       " index "+str(sindex)+\
			      str(short_column == column )+\
			      " is_equal: "+str(is_equal)+"\n"
			if is_equal:
			#if short_column == column:
				return col_ind
		
		return "NoIndex"
		
	def ComputeMooreAutomata(self):
		#dict_alphabet = SetToDict( self.alphabet )
		#dict_index = SetToDict( self.index_set )
	
		(state_set, state_list) = ListToSet( self.table )

		transition = dict()
		for nletter in range(len(self.alphabet)):
			for state in state_set:
				column  = state_list[state]
				scolumn = self.ShiftColumn(column,\
				                       nletter)
				
				sindex  = self.FindIndex(scolumn,\
				                state_list)

				letter = self.alphabet[nletter]
				transition[(letter,state)]=\
				      sindex
		
		output = dict()
		for state in state_set:
			output[state] = state_list[state][0]
		
		zeta = dict()
		for index in range(len(self.index_set)):
			column = self.table[index]

			zeta[self.index_set[index]] = \
			     self.FindIndex(column, state_list)
		
		auto = MooreAutomata(alphabet = self.alphabet,\
				     state = state_set,\
		                     transition = transition,\
				     output = output,\
				     zeta = zeta )
		return auto

					       
		
class MooreAutomata:
	attribute_list = ["state", "alphabet",\
			"transition", "output", "zeta"]

	def __init__(self, **arguments):
		input_data = dict()
		if arguments.has_key("config_file"):
			fp      = arguments["config_file"]
			configf = ConfigParser.ConfigParser()
			configf.readfp(fp)
			for option in MooreAutomata.attribute_list:
				print "Processing field: "+\
				  str(option)+"\n"
				input_data[option] =\
				 eval(configf.get("MooreAutomata",\
				              option)\
				      )
		else:
			input_data = arguments
		
		for field in MooreAutomata.attribute_list:
			self.__dict__[field] = input_data[field]

	def copy(self):
		auto = MooreAutomata( alphabet = self.alphabet,\
		                     state = self.state, \
				     transition = self.transition,\
				     output = self.output,\
				     zeta = self.zeta )
		
		return auto


	def ownprint(self):
		print "Alphabet: "+str(self.alphabet)+"\n"
		print "States: "+ str(self.state)+"\n"
		print "Transitions: \n"
		for input, states in self.transition.iteritems():
  			print "Input: "+str(input[0])+" from state:"+str(input[1])+" to state: "+ str(states)+"\n"
		for states,output in self.output.iteritems():
			print "State :"+str(states)+" output: "+str(output)+"\n"
   
   	def ReachableSet(self):

		initial_states=Set();
		for k,states in self.zeta.iteritems():
			initial_states.add(states)

		rstates = initial_states
   
		is_stop = False;	
		while not is_stop:
			nrstates = rstates.copy()
			for state in rstates:
				for  input in self.alphabet:
					nrstates.add(self.transition[(input,state)])
			is_stop = ( nrstates==rstates )
			rstates = nrstates
  		
  		return rstates 
		
	def GetReachableSet(self):
		if not self.__dict__.has_key("reachable_set"):
			self.reachable_set = \
			  self.ReachableSet()
		
		return self.reachable_set
		
	def ReachableAutomaton(self):
	
		rstate = self.GetReachableSet()

		rtransition = dict()
		routput = dict()

		for state in rstate:
			for input in self.alphabet:
				rtransition[(input,state)] = self.transition[(input,state)]
			routput[state] = self.output[state]
		
  		reach_auto = MooreAutomata(alphabet=self.alphabet,state=rstate, transition=rtransition, output=routput,zeta = self.zeta)
		return reach_auto


 	def ObservableRelation(self):
      		obs_rel = Set()
		for state1 in self.state:
			for state2 in self.state:
				if self.output[state1] == self.output[state2]:
					obs_rel.add((state1,state2))
	  
	  
		is_stop = False
		while not is_stop:
			nobs_rel = Set()
			for pair in obs_rel:
				is_keep = True
		  		for input in self.alphabet:
			        	state1 = self.transition[(input,pair[0])]
					state2 = self.transition[(input,pair[1])]
					if (state1,state2) not in obs_rel:
						is_keep = False
						break
				if is_keep:
					nobs_rel.add(pair)
			
			is_stop = ( nobs_rel == obs_rel )
			obs_rel = nobs_rel 
		return obs_rel			

	def GetObservableRelation(self):
		if not self.__dict__.has_key(\
		            "observable_relation"):
			self.observable_relation = \
			   self.ObservableRelation()
		
		return self.observable_relation
		
	def ObservableAutomaton(self):
		obs_rel = self.GetObservableRelation()

		new_states = dict()
		for states in self.state:
			tt = Set([states])
			new_states[states] = Set([0])
			print "Problem "+str(states)
			new_states[states] = Set([states])
		
		for pair in obs_rel:
			new_states[pair[0]].add(pair[1])

		obs_state = Set()
		obs_output = dict()
		for states in self.state:
			obs_state.add(new_states[states].__as_immutable__())
	                obs_output[new_states[states].__as_immutable__()] = self.output[states]
		obs_transition = dict()	
		for input in self.alphabet:
			for states in self.state:
				obs_transition[\
				(input, \
				new_states[states].__as_immutable__())] = \
				new_states[self.transition[(input,states)]].__as_immutable__()
		
		obs_zeta = dict()
		for index,state in self.zeta.iteritems():
			obs_zeta[index] = \
			  new_states[state].__as_immutable__()
		obs_auto = MooreAutomata(alphabet=self.alphabet, state=obs_state, transition = obs_transition, output = obs_output, zeta = obs_zeta)

		return obs_auto

	def MinimalAutomaton(self):
		rauto = self.ReachableAutomaton()
		mauto = rauto.ObservableAutomaton()
		return mauto

	def IsReachable(self):
		reachable_set = self.GetReachableSet()

		return ( len(reachable_set) == len(self.state))
	
	def IsObservable(self):
		observable_rel = self.GetObservableRelation()

		return ( len(observable_rel) == len(self.state))

	def ComputeState(self, word, initial_state):
		state = initial_state
		for letter in word:
			state = self.transition[(letter, state)]
		
		return state
	
	def ComputeOutput(self, word, initial_state):
		state = self.ComputeState(word, initial_state)

		return self.output[state]
		
		
	def InputOutputFunction(self, length):
		wordlist =WordGenerate(self.alphabet, lenght)
		iomap = dict()
		for (key, value) in self.zeta.iteritems():
			for word in wordlist:
				iomap[(key,word)] = \
				  self.ComputeOutput(word,value)	
		return iomap		  
	

	def ComputeOutput(self, function, iindex, dummy_arg):
		begin_state = self.zeta[iindex]
		end_state   = function[begin_state]
		return self.output[end_state]
	
	def FunctionComposition(self, funct1, funct2):
		funct = dict()
		for state in self.state:
			mstate       = funct2[state]
			end_state    = funct1[mstate]
			funct[state] = end_state

		return funct	
	
	def SizeFunction(self, table_list):
		found_list = []
		for element in table_list:
			if not element in found_list:
				found_list.append(element)
		
		return len(found_list)
		
	
	def HankelTable(self, size=[], stop_size=[], \
	                         index_list=[]):
		
		if size == []:
			size = len(self.state)
		
		if stop_size == []:
			stop_size = len(self.state)
		
		if index_list == []:
			index_list = self.zeta.keys()
		
		
		transition_function = dict()
		
		for letter in self.alphabet:
			map = dict()
			for state in self.state:
				map[state] = \
				self.transition[(letter,state)]
			
			transition_function[letter] = map
		
		identity_function = dict()
		for state in self.state:
			identity_function[state] = state
		
		output_range = [1]

		
		abs_table = HankelTableGenerator(\
			    size_function = self.SizeFunction,\
			    transition_function = \
			                 transition_function,\
			    alphabet = self.alphabet,\
			    output_function = self.ComputeOutput,\
			    index_set = index_list,\
			    identity_map = identity_function,\
			    output_range = output_range,\
			    composition_operator = \
			      self.FunctionComposition)
		
		(table,tsize) = abs_table.HankelTable(size,\
		                                stop_size)
						
		hankt = HankelTable(alphabet = self.alphabet, \
		                    index_set = index_list,\
				    table = table, depth = tsize)
		
		return hankt
		
		
#	def ComputeColumn( self,cword, column, output_list,\
#	                     index, size):
#		if size == 0:
#			column.append(output_list[cword+1][index])
#			
#		else:
#			for nletter in range(len(self.alphabet)):
#				ncword = (cword + 1)*\
#				     len(self.alphabet)+nletter
#				self.ComputeColumn( ncword, \
#				 column, output_list, index, size-1)
#				 
#		return			
#	
#	def UpdateColumnList( self, column_list, size):
#		start_index = len(column_list) -\
#		              len(self.alphabet)**(size - 1)
#		
#		index_range = len(self.alphabet)**size
#
#		for index in range(index_range):
#			for nletter in range(len(self.alphabet)):
#				oword = column_list[\
#				         start_index + index ]
#				
#				nword = (oword+1)*\
#				    len(self.alphabet) + nletter
#
#
#	
#				column_list.append(nword)
#		
#		return
#		
#		
#	def ComputeSubHankelTable( output_list, state_list,\
#	                          column_list, table, size ):
#				  
#		if size == 0:
#			self.UpdateLists(output_list, state_list, 1)
#		else:	
#			start_size = 2*size - 1
#			self.UpdateLists(output_list, state_list,\
#		                               start_size+ 1)
#			self.UpdateLists(output_list, state_list, \
#		                               start_size+ 2)
#		
#		lenght_old = len(column_list)
#		column_list_old = column_list[0:length_old]
#
#		self.UpdateColumnList(self, column_list, size)
#		
#		newcolumns = column_list[\
#		                length_old:len(column_list)]
#		
#		for column_word in newcolumns:
#			for key in index_set:
#				column = []
#				for snum in range(size+2):
#					self.ComputeColumn(\
#						column_word,\
#						column,
#						output_list,\
#						key, snum)
#				
#				table.append(column)
#			
#		col_index = 0	
#		for i in range(length_old):
#			for index in index_set:
#				self.ComputeColumn(\
#				 column_list[i], \
#				 table[col_index], output_list,\
#				 index, size + 1)
#		
#				col_index = col_index + 1
#		
#		return
#		
#		
#
#	def HankelTable(self, length, size,):
#		output_list = []
#		
#		state_list = []
#		
#		for state in self.state:
#			transition = dict([(state, state)])
#			state_list.append(transition)
#
#
#		table = dict()
#
#		depth = 0
#
#		for snum in range(length+1):
#			depth = snum
#			self.ComputeSubHankelTable(\
#			   output_list, table,\
#			   snum)
#			
#			if GetSize(table) == size:
#				break
#		
#		hankt = HankelTable(alphabet = self.alphabet, \
#		                    index_set = self.zeta.keys(),\
#				    table = table, depth = depth)
#		
#		return hankt
#				
#		
