import copy
from own_io import own_print

is_output=False

def IndexToNumber( alphabet, index_set, word, index ):
        number = -1
	d  = len(alphabet)
	d1 = len(index_set)
	#rword = copy.deepcopy(word)i
	#rword.reverse()
	for letter in word:
		number=d*(number+1)+ letter[1]
	return (number+1)*d1+index[1]	

def SetToDict( input_set ):
	index = 0
	dict_input_set = []
 	for element in input_set:
		dict_input_set.append((element, index))
		index = index + 1
	return dict_input_set	
	
def WordGenerate( alphabet, depth ):
	if depth == 0:
		return [[]]
	
	swordlist = WordGenerate(alphabet, depth-1)

	own_print(is_output,"Depth: "+str(depth)+" list:"+str(swordlist)+"\n")

	wordlist = [[]]
	#for letter in alphabet:
	#	wordlist.append([letter])
		
	for word in swordlist:
		for letter in alphabet:
			#word.append(letter)
			wordc = copy.deepcopy(word)
			wordc.append(letter)
			own_print(is_output,"Word2: " + str(wordc)+"\n")
			own_print(is_output,"Word3: " + str(word)+"\n")
			wordlist.append(wordc)
	
	return wordlist
