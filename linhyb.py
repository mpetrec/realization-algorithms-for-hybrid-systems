from hybrid_power import *
from scipy import integrate

class LinearHybridSystem(HybridPower):
	Input_attribute_list = [ "index", "automata", "alphabet",\
	                         "a_matrices", "b_matrices",\
				 "c_matrices", "zeta", "reset_maps"]
	
	Config_file_attrib  = [ "index", "alphabet",\
	                         "b_matrices", "a_matrices",\
				 "c_matrices", "zeta", "reset_maps"]
	

	def __init__(self, **arguments):

		if arguments.has_key("hyb_pow"):
			HybridPower.__init__(self, **arguments)
			return 

		input_data = dict()

		if arguments.has_key("config_file"):
			fp 	= arguments["config_file"]

			configf = ConfigParser.ConfigParser()

			configf.readfp(fp)

			for field in \
			  LinearHybridSystem.Config_file_attrib:
			  	print "Processing field:"+\
				      str(field)+"\n"
			  	input_data[field] = eval(\
			  	     configf.get(\
				       "LinearHybridSystem",field)\
				   )    
			fp.seek(0)	   
			input_data["automata_config_file"] = fp		   
		else:
			for field in LinearHybridSystem.Input_attribute_list:
				input_data[field]=arguments[field]
	
		input_data["index_first"] = input_data["index"]
		input_data["alphabet_disc"] = input_data[ \
		                                "alphabet"]

		input_data["alphabet_cont"] = [ 'e' ]

		a_matrices = dict()
		for (key,value) in input_data[\
		              "a_matrices"].iteritems():
			a_matrices[(key, 'e')] = value 
		
		input_data["a_matrices"] = a_matrices

		index_second = []

		is_index_second_def = False

		b_matrices = dict()

		for (key, value) in input_data[\
		                      "b_matrices"].iteritems():
			if not is_index_second_def:
				is_index_second_def = True
				index_second = range( \
				                 value.shape[1])
			for j in range(value.shape[1]):
				b_matrices[(key,('e',j))] = \
				    reshape( \
				       value[0:value.shape[0], j],\
				       (value.shape[0],1) )
			
		input_data["index_second"] = index_second 	
		input_data["b_matrices"] = b_matrices

		HybridPower.__init__ (self, **input_data)
		
	def ComputeMinimal(self):
		hybpow = HybridPower.ComputeMinimal(self)

		mlinhyb = LinearHybridSystem( hyb_pow = hybpow )

		return mlinhyb
		
	def HybridHankelMatrix(self, cont_length, cont_rank,\
	                       disc_length, disc_rank):
			       
		hyb_hankel = HybridPower.HybridHankelMatrix(\
				self,\
		                cont_length, cont_rank, \
				disc_length, disc_rank)
		
		linhyb_hankel = LinearHybridHankelMatrix(\
		           hyb_hankel = hyb_hankel )
		
		return linhyb_hankel
		
		
	def RightHandSide(self, time, current_state, input):
	        discrete_state = current_state[0]
		cont_state     = current_state[1]
		a_matrix = self.a_matrices[(discrete_state,'e')] #.astype('d')
		b_matrix_list = []
		#print "Discrete state: " + str(discrete_state) + \
	#"Cont. state: "+str(cont_state)+" shape: " + str(cont_state.shape) + "\n"
		input_size = len(self.index_second)
		cont_state_size = self.state_dimensions[discrete_state]

		cont_state = reshape(cont_state,(cont_state_size,1))#.astype('d')
		b_matrix = zeros((cont_state_size,input_size), 'd')
		for i in range(cont_state_size):
			for j in range(input_size):
				b_matrix[i][j]= \
		     	        self.b_matrices[(discrete_state, ('e',j))][i]#.astype('d')
		#print "A matrix: " + array2string(a_matrix)+"\n"
		#print "B matrix: " + array2string(b_matrix)+\
		#" shape : " + str(b_matrix.shape)+ "\n"
		
	        derivative = matrixmultiply(a_matrix, cont_state)+ \
		   matrixmultiply(b_matrix, input(time))
		   
		#print "Time: " + str(time)+ "input: "+\
		#  str(input(time))+"\n"
		#print "Derivative: "+str(derivative)+"\n"   
		return derivative
		#return reshape(derivative,(cont_state_size,))   
			
	def GenerateStateTrajectory(self, state, \
	                         switching, etime, cont_input):
		gtime = 0
		#step_number = 1000
		step_size=0.01
		atol = 1e-8
		rtol = 1e-8
		nsteps = 1000
		method='adams'
		d_state = state[0]
		c_state = reshape(state[1], (self.state_dimensions[d_state],1))#.astype('d')
		traject = []
		for (input,time) in switching:
			def rhside(stime,istate):
				print "Init: "+str(istate)+"\n"
				return self.RightHandSide(stime, (d_state, istate), cont_input)
				
			solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
			traject.append([gtime, (d_state,c_state)])
			print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
			while solver.t  < time + gtime and \
			 solver.successful():
			 	c_state = solver.integrate(solver.t+step_size)
				c_state = reshape(c_state, (self.state_dimensions[d_state],1))#.astype('d')
				traject.append([solver.t,(d_state,c_state)])
					
			c_state = matrixmultiply(self.reset_maps[(input,d_state)], c_state) #.astype('d'))
			d_state =  \
			  self.automata.transition[(input, d_state)]
			
			gtime = gtime + time
			  
		print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
		def rhside(stime,istate):
			#print "Init:"+str(istate)+"stime: "+str(stime)+"type: "+str(istate.typecode())+"\n"
			return self.RightHandSide(stime, (d_state, istate), cont_input)
			
		solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
		traject.append([gtime,(d_state,c_state)])
		while solver.t  < etime + gtime and \
		solver.successful():
			#print "cstate:"+ array2string(c_state) + " discrete state" +str(d_state)+ "\n"
	 		c_state = solver.integrate(solver.t+step_size)
			c_state = reshape(c_state, \
			  (self.state_dimensions[d_state],1))#.astype('d')
			traject.append([solver.t,(d_state,c_state)])

		#stime = gtime
		#time_axis = [gtime]
		#while stime < gtime + etime:
		#	stime = stime+ step_size
		#	time_axis.append(stime)
				
#		 	#	c_state = solver.integrate(solver.t+step_size)
		#atime_axis=reshape(array(time_axis, 'd'),len(time_axis))	
		#c_state=reshape(c_state, self.state_dimensions[d_state])
		#atime_axis=linspace(gtime, gtime+etime,step_number)
		#solution = integrate.odeint(rhside,c_state, atime_axis)
		#for rindex in range(atime_axis.shape[0]):
		#   	stime = atime_axis[rindex]
		#	c_state = transpose(solution[0][rindex,:])
		#	traject.append([stime, (d_state,c_state)])
		
	     	
		return traject

	def GenerateContOutput(self, state, etime, \
	      switching, input ):
	      	state_traject = self.GenerateStateTrajectory(\
		          state, etime, switching, input)
		output_traject = []	  
		for traject_point in state_traject:
		        time = traject_point[0]
			dstate = traject_point[1][0]
			cstate = traject_point[1][1]
			output = matrixmultiply(self.c_matrices[dstate], cstate)
			output_traject.append([time,output])
		
		return output_traject
				  
	      

class LinearHybridHankelMatrix(HybridHankel):
	input_attributes = ["alphabet", "index",\
	                     "cont_output_dimension", \
			     "input_dimension", "hankel_matrix",\
			     "hankel_table_obj",\
			     "ext_hankel_table_obj"]
	
	def __init__ (self, **arguments):
		if arguments.has_key("hyb_hankel"):
			HybridHankel.__init__(self, \
			  hyb_hankel = arguments["hyb_hankel"])
			return
			
		input_data = arguments
		input_data["alphabet_cont"] = ['e']
		input_data["alphabet_disc"] = \
		       arguments["alphabet"]
		input_data["index_first"]  = arguments["index"]
		input_data["index_second"] = range(\
		   arguments["input_dimension"])
		
		HybridHankel.__init__(self, **input_data)
	
	def ComputeLinearHybridSystem(self):
		hybpow = HybridHankel.ComputeHybridPowerRepr(\
		                          self)
		
		linhyb = LinearHybridSystem( hyb_pow = hybpow )

		return linhyb
		
