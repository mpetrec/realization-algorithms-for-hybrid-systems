#!/usr/bin/env python
#import form_pow_repr
from numpy import *
from sets import *
import hankel_matrix
import switched_systems
from dfa_automata import *
import sys,os
import copy



if len(sys.argv) < 2:
	filename = "lin_switch_config"
else:
	filename = sys.argv[1]

if len(sys.argv) < 3:
	switched_systems.def_precision = 1e-2
else:
	switched_systems.def_precision = eval(sys.argv[2])

switched_systems.init_module(switched_systems.def_precision)
fp= open(filename)

linsys = switched_systems.LinearSwitchedSystem(config_file = fp )
     			#discrete_modes=discrete_modes, \
                        #   a_matrices=a_matrices,\
                        #   b_matrices=b_matrices, \
			#   c_matrices=c_matrices, \
			#   initial_states=initial_states)


print "Linsys \n "
linsys.ownprint ()

print "Is Obseravble \n"+ str(linsys.IsObservable())+"\n"+\
      " Is Reachable \n" + str(linsys.IsReachable())+"\n"

mlinsys = linsys.MinimalSystem()

print "Minimal system \n"
mlinsys.ownprint()

 
print "Is Obseravble \n"+ str(mlinsys.IsObservable())+"\n"+\
      " Is Reachable \n" + str(mlinsys.IsReachable())+"\n"
