from numpy import *
from numpy import dot as matrixmultiply
from numpy.linalg import lstsq as linear_least_squares
from own_io import own_print
#from LinearAlgebra import *
#import Matrix
#import MLab
from sets import Set

from word_operations import *

from abstract_hankel_generate import *


is_output=False
#is_output = True

def_precision = 1-30
minEpsilon = 1e-4

def compute_own_svd(matrix,rcond = [], cutoff=[]):
	#print "Matrix :"+str(matrix)+\
	#      "shape "+str(matrix.shape)+"\n"
	if rcond == []:
		rcond = def_precision
	own_print(is_output, "Rcond in svd:"+str(rcond)+"\n")
	try:
		svd_decomp = linalg.svd(matrix)
		own_print(is_output, "SVD return"+\
				 str(svd_decomp)+"\n")
		#if cutoff == []:
		#	cutoff = rcond * maximum.reduce(svd_decomp[1])
		#	cutoff = max([cutoff, minEpsilon])

		cutoff = minEpsilon
	
		is_empty=True
		n=svd_decomp[0].shape[0]
		m=svd_decomp[2].shape[1]
		for i in range(svd_decomp[1].shape[0]):
			own_print(is_output, "i: "+str(i)+" S(i): "+str(svd_decomp[1][i])+" cutoff "+str(cutoff)+" is > :"+str((svd_decomp[1][i] > cutoff))+"\n")
			if svd_decomp[1][i] > cutoff :
				if is_empty:
					U  = reshape(svd_decomp[0][:,i],(n,1))
					VT = reshape(svd_decomp[2][i,:],(1,m))
					S = array([svd_decomp[1][i]], 'd')
					is_empty = False
					
				else:
					U = concatenate((U,
				  	reshape(svd_decomp[0][:,i], (n,1))),1)
					VT = concatenate((VT,
					reshape(svd_decomp[2][i,:], (1,m))),
					0)
					S = concatenate(
					   (S,[svd_decomp[1][i]]),
					   0)
					
		if is_empty:
			return "Zero solution"
	
		S=diag(S)		
		own_print(is_output,"Truncated SVD: " + str((U,S,VT))+"\n")
		return (U,S,VT)			
	except linalg.LinAlgError: 
		print "Problems with SVD\n"
		return
			  
def compute_rank(matrix,rcond=[]):
	if rcond == []:
		rcond = def_precision
        svd_decomp = compute_own_svd(matrix,rcond)
	if svd_decomp == "Zero solution":
		print "Zero solution to SVD\n"
		return (0, zeros((matrix.shape[0],1), 'd'))
		
	U = svd_decomp[0]
	matrix_rank = U.shape[1]
	#own_print(is_output,"Matrix:"+ str(matrix)+"\n"		)
	return (matrix_rank, U)



def base_compute_recursive( start_vectors, maps, \
	                     graph ):
				     
	is_stop = False

	own_print(is_output, "Graph: "+str(graph)+"\n")

	rmatrix = dict()
	orank = dict()
	for (index,value) in start_vectors.iteritems():
		#rmatrix[index] = value
		old_rank        = compute_rank(value)	
		rmatrix[index]  = old_rank[1]
		orank[index]    = old_rank[0]
			
	while not is_stop:
		own_print(is_output,"Reachable: "+ str(rmatrix)+"\n")
		new_rank = dict()
		for (index, rvalue) in rmatrix.iteritems():
			#for input, tmatrix in maps.iteritems():
			list = graph[index]
			#value = rvalue
			new_value = zeros(rvalue.shape, 'd')
			for i in range(rvalue.shape[0]):
				for j in range(rvalue.shape[1]):
					new_value[i][j] = \
					        rvalue[i][j]
			#print "Index: "+str(index)+\
			#          "list: "+str(list)+"\n"
			for element in list:
				input  = element[0]
				sindex = element[1]
				svalue  = rmatrix[ sindex ]
				
				#print "Svalue: "+str(svalue)+"\n"

				nsvalue = matrixmultiply(\
				          maps[input],\
					  svalue)
				#print "Value: "+str(rvalue)+\
				#      " svalue: " +str(svalue)+\
				#      " nsvalue: "+str(nsvalue)+\
				#      " new_value: "+str(new_value)+\
				#      " maps[inputs] "+str(maps[input])+\
				 #     " input "+str(input)+\
				 #    "\n"
				new_value = concatenate( \
			              (new_value , nsvalue) \
		                        ,1)		   
		
			new_rank[index] = compute_rank(new_value)
					  
		is_stop = True			  
		for index in new_rank.keys():			  
			if not new_rank[index][0] == orank[index]:
				is_stop = False
				break
		#if not is_stop:
		for index in new_rank.keys():
			orank[index] = new_rank[index][0]
			rmatrix[index] = new_rank[index][1]
				
  	return rmatrix


def compute_recursive(start_vector, maps):
	is_empty = True
	for index,bs in start_vector.iteritems():
		if is_empty:
			rmatrix = bs
			is_empty = False
		else:	
	    		rmatrix = concatenate(( \
		            rmatrix, bs), 1 )
	is_stop = False

	old_rank = compute_rank(rmatrix)	
	rmatrix = old_rank[1]
	orank = old_rank[0]
	while not is_stop:
		own_print(is_output,"Reachable: "+ str(rmatrix)+"\n")
		for input, tmatrix in maps.iteritems():
			rmatrix = concatenate( \
			   (rmatrix, \
			   matrixmultiply( tmatrix, \
			           rmatrix)) \
		           ,1)		   
		
		
	       	new_rank = compute_rank(rmatrix)
		if new_rank[0] == orank:
			is_stop = True
		else:
			orank = new_rank[0]
		rmatrix = new_rank[1]
				
  	return rmatrix


class Representation:
	
	attributes = ["alphabet", "dimension", "output",\
	              "transition", "zeta", "output_dimension"]
		      
	def copy(self, other):
		for field in Representation.attributes:
			self.__dict__[field] = \
			other.__dict__[field]
		
		return 
		
	def __init__(self, alphabet,
	                transition,output,zeta, **args):
		if args.has_key("copy"):
			Representation.copy(self, args["copy"])
			return
			
		self.alphabet = alphabet
		self.dimension = output.shape[1]
		self.output = output
		self.transition = transition
		self.zeta = zeta
		self.output_dimension = output.shape[0]

	def ownprint(self, oprecision=2, sup_small=1):
		print "Alphabet: "+str(self.alphabet)+"\n"
		#own_print(is_output,"Dimension: "+ str(self.dimension)+"\n")
		print "Transitions: \n"
		for input, matrices in self.transition.iteritems():
  			print "Input: "+str(input)+"\n"
			print " matrix: "+array2string(matrices,precision=oprecision,suppress_small=sup_small)+"\n"
		print "Output:" + array2string(self.output,precision=oprecision,suppress_small=sup_small)+"\n"
		for index,elem in self.zeta.iteritems():
			print "Zeta: index: " +\
			 str(index)+ " elem: "+\
			 array2string(elem,precision=oprecision,suppress_small=sup_small)+"\n"
		
	def ReachableMatrix(self):
		return compute_recursive(self.zeta, self.transition)	
	
	def GetReachableMatrix(self):
		if not self.__dict__.has_key("reachable_matrix"):
			self.reachable_matrix =\
			    self.ReachableMatrix()
		
		return self.reachable_matrix

		
	def ReachableRepresentation(self):
		reachable_matrix = self.GetReachableMatrix()
		return self.ComputeRestrictedRepr( reachable_matrix )
		

	def ComputeRestrictedRepr(self, rmatrix):
            	#nrank = compute_rank( reachable_matrix )
		#reachable_matrix = nrank[1]
		#rdimension = reachable_matrix.shape[1]
		
		rtransition = dict()
		for input in self.alphabet:
			rtransition[input] = matrixmultiply( \
				matrixmultiply( \
				    transpose( rmatrix ),\
					self.transition[input] \
					),\
					rmatrix \
				) 
		rzeta = dict()	
		for (index, element) in self.zeta.iteritems():
			rzeta[index] = matrixmultiply( \
			      transpose( rmatrix ), \
			      element \
			      )
		
		routput = matrixmultiply(self.output, rmatrix)
		
		rrepr = Representation(self.alphabet, rtransition, routput, rzeta)

		return rrepr
		
	def ObservabilityMatrix(self):
		p = self.output.shape[0]
		n = self.output.shape[1]
		omaps = dict()
		for i in range(p):
	 		omaps[i] = reshape(self.output[i,:], (n,1))
		
		otrans = dict()

		for input,tmatrix in self.transition.iteritems():
			otrans[input] = transpose( tmatrix )
		
		t_obs_matrix = compute_recursive( omaps, otrans )

		return t_obs_matrix 

	def GetObservabilityMatrix(self):
		if not self.__dict__.has_key(\
		                   "observability_matrix"):
			self.observability_matrix = \
			  self.ObservabilityMatrix()
		
		return self.observability_matrix
		
	def ObservableRepresentation(self):
		obs_mat = self.GetObservabilityMatrix()
		return self.ComputeRestrictedRepr( obs_mat )
	
	def MinimalRepresentation(self):
		rrepr = self.ReachableRepresentation()
		mrepr = rrepr.ObservableRepresentation()
		return mrepr

	def IsObservable(self):
		obs_mat = self.GetObservabilityMatrix()
		own_print(is_output, "Obs. matrix "+str(obs_mat)+"\n")
		return ( obs_mat.shape[1] == self.dimension )

	def IsReachable(self):
		rreach = self.GetReachableMatrix()
		own_print(is_output, "Reach. matrix "+str(rreach)+"\n")
		return ( rreach.shape[1] == self.dimension )

	def ComputeMatrixProduct(self, word):
	 
		result = identity(self.dimension,'d')
	 	for letter in word:
			#own_print(is_output,"Letter: "+str(letter)+"first com:"+str(letter[0])+"\n")
	 		result = matrixmultiply( 
		             self.transition[letter], result )
			#own_print(is_output,"Result:"+str(result)+"trans:"+str(self.transition[letter[0]])+"\n" )
	
	 	return result

	def ComputeMarkovParameter(self, word, index, oindex):
	  	matrix = self.ComputeMatrixProduct(word)

		#own_print(is_output,"Word: "+str(word)+"index: "+str(index)+"matrix:"+str(matrix)+"\n")

		matrix = matrixmultiply(self.output[oindex,:], matrix)
		matrix = matrixmultiply(matrix, self.zeta[index] )

		return matrix
		
	def BruteForceHankelMatrix(self, size=[]):
		own_print(is_output,"Size: " + str(size)+"\n")
		if size == []:
			size = self.dimension	
		rsize = size + 1
		csize = size  
		#oalphabet = SetToDict( self.alphabet )
		rwordset = WordGenerate( self.alphabet, rsize )
		
		cwordset = WordGenerate( self.alphabet, csize )

		own_print(is_output,"columns: "+ str(cwordset)+"row: "+str(rwordset)+"\n")
		index_list = []
		for element,key in self.zeta.iteritems():
			index_list.append((element,key))	
		
		print "Index_list_brute:"+str(index_list)+"\n"
		hankm = []
		for cword in cwordset:
			own_print(is_output, "Column word: "+\
			    str(cword)+"\n")
			for index in index_list:
				column = []
				for rword in rwordset:
					word = copy.deepcopy(cword)
					word.extend(rword)
					own_print(is_output,"Word: "+str(word)+"\n")
					for i in range(self.output.shape[0]):
						mp=self.ComputeMarkovParameter(word,index[0],i)
						own_print(is_output,"MarkovParam:"+str(mp)+" i: " +str(i)+"index[0]:"+str(index[0])+"\n"	)
						column.append(mp[0])
				own_print(is_output,"Column: "+str(column)+"\n"		)
				hankm.append(column)	
		
		hank_matrix = transpose(array(hankm, 'd'))
		return hank_matrix

	def AltComputeMarkovParameter(self, amatrix, index,i):
		matrix1 = matrixmultiply(amatrix, \
		              self.zeta[index])
		
		matrix2 = matrixmultiply(self.output[i,:],\
		                       matrix1)
				       
		return matrix2
	

	def UpdateColumnIndexList(self, clind_list, size):
		if size == 0:
			clind_list.append(-1)
			return
		start_index = len(clind_list) -\
		              len(self.alphabet)**(size-1)
		index_range = len(self.alphabet)**(size-1)


		for element in range(index_range):
			element = clind_list[element+start_index]
			for j in range(len(self.alphabet)):
				clind_list.append(\
			          (element+1)*len(self.alphabet)+\
			 	  j)
		
		return

		
	def SetColumn(self, column, matrix_list, begin_index, \
	                             size, column_index):		     
		if size == 0:
			for l in range(self.output.shape[0]):
				column.append(\
				  matrix_list[begin_index+1][\
				  (column_index,l)])
		
		else:
			for i in range(len(self.alphabet)):
				index = (begin_index+1)*\
				      len(self.alphabet) + i
				self.SetColumn(column, \
				 matrix_list, index, size-1,\
				 column_index )
		
		return
	

	def ComputeSubHankelMatrix(self, matrix_list, a_matrix_list,\
	                      size,hankm, index_set, column_list):
			      #i#icolumn,\
			      #irow ):
			
		if size == 0:	
			self.ComputeMatrixList(\
			 a_matrix_list, matrix_list, index_set,\
			 1)
		else:	
			start_extend = 2*size-1
			for i in range(2):
				self.ComputeMatrixList(\
					a_matrix_list,\
		                        matrix_list, index_set,\
					start_extend + i + 1  )
		
		len_old = len(column_list)
		column_list_old = column_list[0:len_old]
		self.UpdateColumnIndexList(column_list,size)

	     	#new_col_number = len(self.alphabet)**size	
		new_columns = column_list[len_old:len(column_list)]
		own_print(is_output, "New columns \n"+str(new_columns)+"\n")
		for column_index in new_columns:
			for index in index_set:
				column = []
				for wlength in range(size+2):	
					self.SetColumn(column,\
					  matrix_list,\
					  column_index,wlength,\
					  index)		   
				own_print(is_output,\
				      "Column: "+str(column)+ \
				      "\n")
				hankm.append(column)		
			 
			 
		col_index = 0
		for colind in column_list_old:
			for index in index_set:
				self.SetColumn(hankm[col_index],\
				   matrix_list, colind, \
				   size+1,index) 
				own_print(is_output,\
				"Col_index:"+str(col_index)+\
				      "\n")
				col_index = col_index + 1

					
		#icolumn = (icolumn+1)*len(self.alphabet)
		#irow    = irow * len(self.alphabet) + 1
		
	#return icolumn, irow

		return
	
	
	
	def ComputeAMatrixList(self, a_matrix_list, \
	                           start_index, index_range):
				   
		new_a_matrix_list = []
			
		own_print(is_output, "index_range: " +\
		     str(index_range)+"\n"\
		       + " start_index "+str(start_index)+\
		       " size_a_matrix_list "+str(len(a_matrix_list))+"\n")
		count = len(a_matrix_list)	
		for index in  range(index_range):
			for letter in self.alphabet:
				matrix = a_matrix_list[\
				          start_index + \
					  index  ]
					  
				nmatrix = matrixmultiply(\
					  self.transition[letter],
					  matrix
					  )
					  
				new_a_matrix_list.append(\
				  nmatrix)
				own_print(is_output,\
				"index: "+str(index+start_index)+\
				      " letter:"+str(letter)+\
				      " count: "+str(count)+"\n")
		                count = count + 1
		a_matrix_list.extend(new_a_matrix_list)
		

		return 
	
	def ComputeMatrixList(self, a_matrix_list, matrix_list,\
	                      index_set, size ):
		
		index_start = len(a_matrix_list) - \
		              len(self.alphabet)**(size-1) 			  
		index_range = len(self.alphabet)**(size-1)

		index_start_markov = len(a_matrix_list)

		self.ComputeAMatrixList(a_matrix_list, \
		                     index_start, index_range )
		
		self.ComputeMarkovMatrixList(a_matrix_list,\
		      matrix_list, index_set, \
		      index_start_markov, \
		             index_range*len(self.alphabet) )
		
	
		return 

	def OLdComputeMatrixList(self, a_matrix_list ):
		
		new_a_matrix_list = []
			
		for letter in self.alphabet:
			for matrix in  a_matrix_list:
				nmatrix = matrixmultiply(\
					  self.transition[letter],\
					  matrix)
				new_a_matrix_list.append(\
				  nmatrix)
		
		a_matrix_list = [identity(self.dimension,'d')]
		a_matrix_list.extend(new_a_matrix_list)

		return 
		
	def ComputeMarkovMatrixList(self, \
	                    a_matrix_list, matrix_list, index_set,\
	                    start_index, index_range ):
		for matrix_index in range(index_range):
			melement = dict()
			for index in index_set:
				for i in \
				  range(self.output.shape[0]):
					mp = self.\
					 AltComputeMarkovParameter(\
					 a_matrix_list[\
					 start_index + \
					 matrix_index ],\
					 index,i)
					melement[(index,i)] = mp[0]
		
			matrix_list.append(melement)
		
		return 
		
	def GetSqSubMatrix(self, hankel_list, size ):
		word_length = len(self.alphabet)**(size+1)
		row_length = word_length * self.output_dimension
		print "Row_length:"+str(row_length)+" size:"+\
		      str(size)+"\n"
		sub_hankel_list = []
		for col in hankel_list:
			sub_col = col[0:(len(col) - row_length)+1]
			sub_hankel_list.append(sub_col)
		
		sub_hankel_matrix = array( sub_hankel_list, 'd')
		return sub_hankel_matrix
		
	def ComputeRankHankel(self, hankel_list):
		matrix = array(hankel_list, 'd')
		rank   = compute_rank(matrix)
		return rank[0]
	
	def ComputeMatrixComposition(self, matrix1, matrix2):
		matrix = matrixmultiply( matrix1, matrix2 )
		return matrix
		
	def ComputeOutput(self, matrix, index, i):
		matrixo = self.AltComputeMarkovParameter(\
		              matrix, index, i)
		return matrixo[0]
		
			
	def HankelTableGen(self, index_list):

		identity_matrix = identity(self.dimension, 'd')
		output_range = range(self.output_dimension)

		#print "Hankel table gen start\n"

		abs_hank_gen = HankelTableGenerator(\
		           size_function = self.ComputeRankHankel,\
			   transition_function = self.transition,\
			   output_function = \
			   self.ComputeOutput,\
			   index_set = index_list,\
			   alphabet = self.alphabet,\
			   identity_map = identity_matrix,\
			   composition_operator = \
			    self.ComputeMatrixComposition,
			   output_range = output_range ) 
		#print "Hankel table gen end\n"
		
		return abs_hank_gen
		
	
	def HankelMatrix(self, size =[], stop_rank=[],\
	                        index_list = []):
	
		if size == []:
			size = self.dimension	
		if stop_rank == []:
			stop_rank = self.dimension
		if index_list == []:
			index_list = self.zeta.keys()

		if  not self.__dict__.has_key("abs_hank_gen"):
			self.abs_hank_gen = self.HankelTableGen(index_list)
		

		if self.__dict__.has_key("hankel_table"):
			if self.hankel_table_size >= size:
				hankel_matrix = self.HankelSubMatrix(size+1,size)
				return (hankel_matrix, size, index_list)
				
                    
		#print "Hankel table gen again\n"
		(self.hankel_table, self.hankel_table_size) = self.abs_hank_gen.HankelTable(size, stop_rank)		
			
					
		hankel_matrix = array(self.hankel_table, 'd')
		hankel_matrix = transpose(hankel_matrix)

		return (hankel_matrix, self.hankel_table_size, index_list)

	def HankelSubMatrix(self, size_r,size_c):
		index_list = self.zeta.keys()

		if size_r < size_c:
			max_size = size_c
		else:
			max_size = size_r

		if not self.__dict__.has_key("abs_hank_gen"):
			self.abs_hank_gen = self.HankelTableGen(index_list)
		
		if not (self.__dict__.has_key("hankel_table")):
			(self.hankel_table, self.hankel_table_size) = \
                            self.abs_hank_gen.HankelTable(max_size+1, self.dimension + 1)

		elif not (self.hankel_table_size > max_size + 1):
			(self.hankel_table, self.hankel_table_size) = \
                           self.abs_hank_gen.HankelTable(max_size+1, self.dimension + 1)
		
		sub_matrix_list = self.abs_hank_gen.GetSubHankel(\
					self.hankel_table,size_c,size_r)
		
 		sub_matrix = array(sub_matrix_list, 'd')	
		sub_matrix = transpose(sub_matrix)

		return sub_matrix

	def FirstPartialReal(self):
	
		for size in range(self.dimension+1):
			hank1  = self.HankelSubMatrix(size,size)
			
			hank2 = self.HankelSubMatrix(size+1,size)

			hank3 = self.HankelSubMatrix(size,size+1)

			(n1,U1) = compute_rank(hank1) 
			(n2,U2) = compute_rank(hank2)
			(n3,U3) = compute_rank(hank3)

			#print "n1 n2 n3: " + str(n1)+" "+str(n2)+" "+ str(n3) + "\n"

			if n1==n2 and n2==n3:
				return (hank2, size, self.abs_hank_gen.index_set)

		
		return (hank1,size,self.abs_hank_gen.index_set)
			
	
	def OldHankelMatrix(self, size=[], stop_rank=[], index_list=[]):
		own_print(is_output,"Size: " + str(size)+"\n")
		if size == []:
			size = self.dimension	
		if stop_rank == []:
			stop_rank = self.dimension
		if index_list == []:
			index_list = self.zeta.keys()
	  		#for (index, elem) in self.zeta.iteritems():
			#	index_list.append(index)
				
		own_print(is_output,\
		"Index_list: "+str(index_list)+"\n")
		a_matrix_list = [identity(self.dimension)]
		matrix_list = []
               	#self.ComputeAMatrixList(a_matrix_list, 0, 1)
		#print "A_matrix_list:"+str(a_matrix_list)+"\n"
		self.ComputeMarkovMatrixList(a_matrix_list,\
		                   matrix_list, index_list, 0, 1)
			
		
		column_index_list = []	
		hankm = []		
		
		hankel_matrix = array([0],'d') #Ugly, dummy assignment
		                           #for defining the
					   #variable 
		
		hankel_matrix_size = 0
		for   snum in range(size+1):
			own_print(is_output, "size: " + str(snum)+\
			      "column_list:"+\
			      str(column_index_list)+\
			      "\n")
			hankel_matrix_size = snum     
			own_print(is_output, "Hankel_matrix_size:"+\
			   str(hankel_matrix_size)+"\n")
			self.ComputeSubHankelMatrix(\
			 	matrix_list, \
				a_matrix_list, snum, hankm,\
			 	index_list, column_index_list) # icolumn, irow)
			
			own_print(is_output,\
			"matrix_list:"+str(matrix_list)+"\n"\
			       + "a_matrix_list: " +str(a_matrix_list)+"\n")
			own_print(is_output,\
			"Hankel list\n"+str(hankm)+"\n")
			sub_hmatrix = self.GetSqSubMatrix(hankm, snum)
			own_print(is_output, "Hankel matrix\n"+str(hankel_matrix)+"\n")
			
			rank = compute_rank(sub_hmatrix)

			own_print(is_output, 
			"Rank of the submatrix: "+str(rank[0])+\
			     " Sub-matrix\n"+\
			   array2string(sub_hmatrix, precision=2,\
			                 suppress_small=1)+"\n")
					 

			if rank[0] == stop_rank:
				break
		
		hankel_matrix = array(hankm,'d')
		hankel_matrix = transpose(hankel_matrix)
		return (hankel_matrix, hankel_matrix_size, index_list)

def compute_hadamard_product(repr1, repr2):
	# Good to check compatibility of repr1,repr2
	state_dimension = repr1.dimension * \
	                  repr2.dimension
	
	# Watch out with this 
	output_dimension = repr1.output.shape[0]
	
	alphabet = repr1.alphabet 

	matrices = dict()
	for letter in alphabet:
		matrices[letter] = zeros( (state_dimension, \
		                         state_dimension ), 'd' )
		for i in range( repr1.dimension ):
			for j in range( repr1.dimension):
				for l in range( repr2.dimension):
					for k in range( repr2.dimension):
						matrices[letter][\
				     			i*repr2.dimension + k, \
				     			j*repr2.dimension + l]= \
				 			repr1.transition[letter][i,j]*\
				 				repr2.transition[letter][k,l]
		
	output = zeros( (output_dimension, state_dimension), 'd')	
	for p in range( output_dimension ):
		for i in range( repr1.dimension ):
			for l in range( repr2.dimension):
				output[p,i*repr2.dimension + l]=\
				  repr1.output[p,i]*\
				  repr2.output[p,l]
	
	zeta = dict()
	for key in repr1.zeta.keys():
		zeta[key]=zeros((state_dimension,1),'d')
		for i in range( repr1.dimension ):
			for j in range( repr2.dimension):
				#print "Index, i"+ str(i)+\
				#" j, "+str(j) +\
				#" i*2repr2.dimension+j:"+\
				#str(i*repr2.dimension + j)+\
				#"key: "+str(key)+\
				#" repr1.zeta " + \
				#str(repr1.zeta[key][i])+\
				#" rep2.zeta "+ \
				#str(repr2.zeta[key][j])+\
				#"\n"
				
				zeta[key][i*repr2.dimension + j]=\
				  repr1.zeta[key][i]* repr2.zeta[key][j]
				  
				#print "Zeta: key: "+\
				#    str(key)+\
				#    " value: "+\
				#    str(zeta[key][i*repr2.dimension + j])+" product :" + str(repr1.zeta[key][i]*repr2.zeta[key][j])+"\n"
				
	
	repr = Representation( alphabet, matrices, output, zeta )

	return repr
	
#			routput[state] = self.output[state]
#		
#  		reach_auto = MooreAutomata(self.alphabet,rstate, rtransition, routput,self.zeta)
#		return reach_auto
#
#
# 	def ObservableRelation(self):
#      		obs_rel = Set()
#		for state1 in self.state:
#			for state2 in self.state:
#				if self.output[state1] == self.output[state2]:
#					obs_rel.add((state1,state2))
#	  
#	  
#		is_stop = False
#		while not is_stop:
#			nobs_rel = Set()
#			for pair in obs_rel:
#				is_keep = True
#		  		for input in self.alphabet:
#			        	state1 = self.transition[(input,pair[0])]
#					state2 = self.transition[(input,pair[1])]
#					if (state1,state2) not in obs_rel:
#						is_keep = False
#						break
#				if is_keep:
#					nobs_rel.add(pair)
#			
#			is_stop = ( nobs_rel == obs_rel )
#			obs_rel = nobs_rel 
#		return obs_rel			
#
#	def ObservableAutomata(self):
#		obs_rel = self.ObservableRelation()
#		new_states = dict()
#		for states in self.state:
#			new_states[states] = Set(states)
#		
#		for pair in obs_rel:
#			new_states[pair[0]].add(pair[1])
#
#		obs_state = Set()
#		obs_output = dict()
#		for states in self.state:
#			obs_state.add(new_states[states])
#	                obs_output[new_states[states].__as_immutable__()] = self.output[states]
#		obs_transition = dict()	
#		for input in self.alphabet:
#			for states in self.state:
#				obs_transition[(input, new_states[states].__as_immutable__())] = new_states[self.transition[(input,states)]]
#		
#		obs_zeta = dict()
#		for index,state in self.zeta.iteritems():
#			obs_zeta[index] = new_states[state]
#		obs_auto = MooreAutomata(self.alphabet, obs_state, obs_transition, obs_output, obs_zeta)
#
#		return obs_auto
#
