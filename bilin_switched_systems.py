from  numpy import *
import  form_pow_repr
from dfa_automata import *
import hankel_matrix
from own_io import own_print
import ConfigParser


#def_precsion = 1e-2
hankel_matrix.def_precision = def_precision

form_pow_repr.def_precision = def_precision

def init_module(def_precision = 1e-2 ):
        hankel_matrix.def_precision = def_precision
        form_pow_repr.def_precision = def_precision
	return
			

class BilinSwitchDFA(BaseDFA):
	def __init__ (self, **arguments):
			#alphabet, states,\
	        #transition, accepting_states,\
		#initial_state, input_dimension, output_dimension,\
		#keys
		#):
		
		input_data = dict()

		if arguments.has_key("config_file"):
			fp = arguments["config_file"]

			conf = ConfigParser.ConfigParser()

			conf.readfp(fp)

			input_data_list =\
			  conf.items("BilinSwitchDFA")

			for element in input_data_list:
				input_data[element[0]] = \
				  eval(element[1])		
		else:
			input_data = arguments	

				  
		self.input_dimension  = input_data[\
				     "input_dimension"]
		self.output_dimension = \
		          input_data["output_dimension"]
		
		zeta = dict()

		for key in input_data["keys"]:
			zeta[key] = input_data["initial_state"]
		
		dfa_alphabet = []
		accepting_states_set = []
	 	for letter in input_data["alphabet"]:
			dfa_alphabet.append((letter,0))
			for i in range(self.input_dimension):
				dfa_alphabet.append((letter,i+1))

			lstates_set=[]
			
			for state in input_data["states"]:
				if input_data["transition"][\
				   (letter,state)] in \
				   input_data[\
				       "accepting_states"]:
				   	lstates_set.append(state)
			
			#if len(lstates_set) > 0 : 	
			accepting_states_set.append(\
				                    lstates_set)
		
		dfa_transition = dict ()

		for (key,value) in \
			input_data["transition"].iteritems():
			for i in range(self.input_dimension+1):
				dfa_transition[((key[0],i),\
				    key[1])] = value
				
		BaseDFA.__init__(self,dfa_alphabet, \
				input_data["states"],\
		 dfa_transition,\
		        accepting_states_set, zeta)
	
	def DFA2Representation(self):
		return BaseDFA.BaseDFA2Representation(self,\
		                            self.output_dimension)
		
	
def HankelMatrixToBilinSwitchConst(hankel_matrix, input_dimension,\
                            output_dimension, discrete_modes, \
			    index_set, length, automata):
	bilinsys = HankelMatrixToBilinSwitch( hankel_matrix,\
	                     input_dimension, output_dimension,\
			     discrete_modes, index_set, length)
	
	bilinsys_const =  BilinearSwitchedSystemConst(\
	                     bilinswitch = bilinsys, \
			     automata = automata )

	return bilinsys_const

def HankelMatrixToBilinSwitch(hmatrix, input_dimension,\
                            output_dimension, discrete_modes, \
			    index_set, length, precision=def_precision):
	
	index = []
	for element in index_set:
		index.append(element)
	
	alphabet = []
	for dstate in discrete_modes:
		for i in range( input_dimension +1):
			alphabet.append((dstate, i))
			
	odimension = output_dimension * len(discrete_modes)
	hankm = hankel_matrix.HankelMatrix(\
	                     alphabet, odimension,\
	                     hmatrix, index, length)
	
	repr = hankm.ComputeRepresentation(precision)

	bilinsys = ReprToBilSwitchSys(repr, input_dimension,\
	                                  output_dimension,\
					  discrete_modes )
	
	return bilinsys


def ReprToBilSwitchSys(repr, input_dimension,\
                         output_dimension, discrete_modes ):
			 
	a_matrices = dict()
	c_matrices = dict()
	b_matrices = dict()

	zeta = repr.zeta.copy()
	start_index = 0
	for key in discrete_modes:
		a_matrices[key]=repr.transition[(key,0)]
		
		b_matrices[key] = []
		for i in range(input_dimension):
			b_matrices[key].append(\
			  repr.transition[(key,i+1)])
		
		end_index = start_index+\
		  output_dimension
		c_matrices[key] = repr.output[start_index:end_index,:]
		start_index=end_index

	bilinswitch= BilinearSwitchedSystem(discrete_modes=\
	   discrete_modes,\
	              a_matrices=a_matrices, \
		      b_matrices=b_matrices ,\
		      c_matrices=c_matrices,\
		      initial_states = zeta)
		      
	return bilinswitch

class BilinearSwitchedSystem:

	attribute_list = ["discrete_modes", \
	                  "a_matrices", "b_matrices", \
			   "c_matrices", "initial_states", \
			   "state_dimension", "input_dimension",\
			   "output_dimension", "zeta_index_list"]
	
	constructor_attribute_list = ["discrete_modes", \
	                   "a_matrices", "b_matrices", \
			   "c_matrices", "initial_states" ]
			   
	def check_object(self):
		return


	def copy(self, bilinswitch):
		for attrib in BilinearSwitchedSystem.attribute_list:
			self.__dict__[attrib] =  \
			  bilinswitch.__dict__[attrib]
	
	def __init__(self, **arguments):
	         #discrete_modes, a_matrices, b_matrices, \
                 #c_matrices, initial_states, is_check=True):
		
		input_data = dict()
		if arguments.has_key("config_file"):
			fp = arguments["config_file"]

			conf = ConfigParser.ConfigParser()

			conf.readfp(fp)

			input_data_list = conf.items(\
				   "BilinearSwitchedSystem")
			
			for element in input_data_list:
				print "Element: "+str(element)+"\n"
				input_data[element[0]] = \
					eval(element[1])	
		else:
			input_data = arguments
			
		if arguments.has_key("copy"):
			BilinearSwitchedSystem.copy(self,\
			  input_data["copy"])
		
		else:
		#self.discrete_modes = discrete_modes
		#self.a_matrices = a_matrices
		#self.b_matrices = b_matrices
		#self.c_matrices = c_matrices
		#self.initial_states = initial_states
			for key in \
			 BilinearSwitchedSystem.constructor_attribute_list:
			 	self.__dict__[key] = \
				  input_data[key]
				  
			self.state_dimension = self.a_matrices[self.discrete_modes[0]].shape[0]
			self.input_dimension = len(self.b_matrices[self.discrete_modes[0]])
			self.output_dimension = self.c_matrices[self.discrete_modes[0]].shape[0]
			self.zeta_index_list = []
			for keys in self.initial_states.keys():
				self.zeta_index_list.append(keys)

		
		print "State dimension "+ \
		str(self.state_dimension)+\
		" output dimension "+ \
		str(self.output_dimension)+\
		" input dimension "+ \
		str(self.input_dimension)+"\n"
		#if is_check:
		#	self.check_object()
		
		
		
	def ownprint(self, oprecision=2, supp_small=1):
		print "Discrete modes:"+ str(self.discrete_modes) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ \
				str(mode) + "\n "+ \
				"A matrix:\n "+ \
				array2string(self.a_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n"
			for i in range(self.input_dimension):
				print "B matrix:\n "+\
					      " input "+\
					      str(i)+" matrix "+\
				              array2string(\
					      self.b_matrices[\
					      mode][i],\
				              precision = \
					      oprecision, \
				              suppress_small=\
					      supp_small)+"\n "
			print 	"C matrix:\n "+\
				array2string(self.c_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n"
		for (key,value) in self.initial_states.iteritems(): 
			print "Initial state: "+ \
			array2string(value,\
				precision = oprecision, \
				suppress_small=supp_small)\
			        +"\n"

	def ComputeRepresentation(self):
		is_empty = True

		self.initial_states
		
		alphabet = []
		
		zeta = dict()
		for (key, value) in self.initial_states.iteritems():
			zeta[key] = reshape(\
			   self.initial_states[key],\
			   (self.state_dimension,1))
		
		transition = dict()
		for mode in self.discrete_modes:
			alphabet.append((mode,0))
			if is_empty:
				output=reshape(\
				  self.c_matrices[mode], \
				  (self.output_dimension,\
				   self.state_dimension))
				is_empty = False
			else:
			  	output=concatenate((output, \
				  self.c_matrices[mode]),0)
			
			transition[(mode,0)] = \
			          self.a_matrices[mode]
		        for i in range(self.input_dimension):
				transition[(mode,i+1)] =\
				   self.b_matrices[mode][i]
				alphabet.append((mode,i+1))	   
				
			
	        #print "Output: " + str(output)+"\n"		
		repr = Representation(alphabet, \
	            transition, output, zeta )
	
		return repr
		
	def ReachableSystem(self):
		repr  = self.ComputeRepresentation()
		rrepr = repr.ReachableRepresentation()
		return ReprToBilSwitchSys( rrepr,self.input_dimension, self.output_dimension. self.discrete_modes )

	def ObservableSystem(self):
		repr  = self.ComputeRepresentation()
		orepr = repr.ObservableRepresentation()
		return ReprToBilSwitchSys( orepr, self.input_dimension, self.output_dimension, self.discrete_modes )
	
	def MinimalSystem(self):
		repr  = self.ComputeRepresentation()
		mrepr = repr.MinimalRepresentation()
		return ReprToBilSwitchSys( mrepr, self.input_dimension, self.output_dimension, self.discrete_modes )

 	def IsObservable(self):
		repr = self.ComputeRepresentation()
		return repr.IsObservable()

	def IsReachable(self):
		repr = self.ComputeRepresentation()
		return repr.IsReachable()

	def HankelMatrix(self,size=[], stop_rank=[], index_list=[]):
		repr = self.ComputeRepresentation()
		if index_list==[]:
			index_list = self.zeta_index_list
			
		return repr.HankelMatrix(size, stop_rank, index_list)
	 	
 
class BilinearSwitchedSystemConst(BilinearSwitchedSystem):
	def __init__(self, \
	    #discrete_modes, \
	    #a_matrices,\
	    #b_matrices, \
	    #c_matrices, initial_states, 
	    # language,
	    **arguments):

		input_data = dict()

		if arguments.has_key("config_file"):
			fp = arguments["config_file"]

			BilinearSwitchedSystem.__init__(\
				self, config_file = fp )

			fp.seek(0)
			
			self.automata = \
			   BilinSwitchDFA(config_file = fp )
		
			return 
		else:
			input_data = arguments
	
		if input_data.has_key("bilinswitch"):
			BilinearSwitchedSystem.__init__(self, \
		  	copy=input_data["bilinswitch"])
		
		else:
			args = dict()
			for key in BilinearSwitchedSystem.constructor_attribute_list:
		 		args[key] = input_data[key]
			
			BilinearSwitchedSystem.__init__(self,\
					*args)
		
		if input_data.has_key("automata"):
			self.automata = input_data["automata"]
		else:	
			self.automata = BilinSwitchDFA(\
			   self.discrete_modes,
		 	   input_data["language"].states, \
			   input_data["language"].transition,\
		           input_data["language"].accepting_states,\
		           input_data["language"].initial_states,\
		  	  self.input_dimension, \
		          self.output_dimension, self.initial_states.keys())
		
			
		
		 
	def ownprint(self):
		BilinearSwitchedSystem.ownprint(self)
		print "Automata: \n"
		self.automata.ownprint()

	def ComputeRepresentation(self):
		repr1 = BilinearSwitchedSystem.ComputeRepresentation(\
		            self )
		
		repr2 = self.automata.DFA2Representation()

	        #print "Repr1:"+" dimension: " +\
		#str(repr1.dimension )+"\n"
		#repr1.ownprint()
		#print "Repr2:"
		#repr2.ownprint()

		repr = compute_hadamard_product( repr1, repr2 )
		 
		#print "Before returning\n"
		#repr.ownprint()
		return repr
	
	def MinimalSystem( self ):
		repr = self.ComputeRepresentation()

		#print "Representation\n"

		repr.ownprint()

		#print "Minimize\n"
		mrepr = repr.MinimalRepresentation()

		#print "Minimal repr\n"
		mrepr.ownprint()
		
	
		bilinsw = ReprToBilSwitchSys( mrepr, \
		         self.input_dimension, \
			 self.output_dimension, \
			 self.discrete_modes )
		
		
		
		#print "Linsw:\n"
		#linsw.ownprint()
		bilinswconst = BilinearSwitchedSystemConst(\
				bilinswitch=bilinsw,\
				automata=self.automata)
		#linswconst.ownprint()		
		return  bilinswconst
	
	#def HankelMatrix(self,size=[], stop_rank=[]):
	#	repr = BilinearSwitchedSystemConst.ComputeRepresentation(self)
	#	return repr.HankelMatrix(size, stop_rank)
		
