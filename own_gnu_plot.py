def own_gnu_plot(**arguments):
	plot_vector = []

	g = arguments["plot"]

	g._clear_queue()
	row_size = arguments["outputs"]

	data_list = arguments["data"]

	print "Data_list"+str(len(data_list))+"\n"
	
	#decomp_data_list = []
	decomp_data_list_one_row = []
	for index in range(row_size):
		data_key = 0
		for vector in data_list:
			plot_vector_element = []
			for data_element in vector:
				plot_vector_element.append((data_element[0],data_element[1][index]))
			title_string = "y["+str(data_key)+"]["+str(index)+"]"
			plot_item=Gnuplot.PlotItems.Data(plot_vector_element, with="lines", title=title_string)
			data_key = data_key + 1
	
			#decomp_data_list_one_row.append(plot_item)
			g._add_to_queue([plot_item)

	print "Decomp length"+str(len(decomp_data_list_one_row))+"\n"
	g.refresh()

	return
