from numpy import *
from form_pow_repr import *
from automata import *
from hankel_matrix import *

import hybrid_power

class ContDiscreteOutput:
	Epsilon = 0.000001
	def __init__(self, list, discrete_output):
		self.list = list
		self.discrete_output = discrete_output
	
	def ownprint(self):
		print "Cont. value: "+str(self.list)+"\n"
		print "Disc. value: "+str(self.discrete_output)+"\n"
	
	def __repr__ (self):
		return "Cont.output: " + str(self.list) +"\n"\
		        + \
		  "Disc. output: "+str(self.discrete_output)+"\n"

	 
	def __eq__(self, other):
		if not self.discrete_output == other.discrete_output:
			return False

		if not len(self.list) == len(other.list):
			return False
		
		for i in range(len(self.list)):
			#if not self.list[i][0] == other.list[i][0]:
			#	return False
			length = range(len(self.list[i]))
			for j in length:
				element = fabs(self.list[i][j] - \
			           other.list[i][j])
				if  (element > \
				 ContDiscreteOutput.Epsilon).any():
					return False
		return True


def ReprFromHybridPower( hybrid_power ): 
	state_dimension = 0

	state_index = dict()
	for (key,value) in hybrid_power.state_dimensions.iteritems():
		state_index[key] = (state_dimension,\
			state_dimension + value )
  		state_dimension = state_dimension + value

	dstate_index = dict()
	for index in hybrid_power.index_second:
		for dstate in hybrid_power.discrete_states:
			dstate_index[(dstate, index)] = (\
			     state_dimension, \
			     state_dimension + 1 )
			state_dimension = state_dimension+1
		
	alphabet =[]
	for letter in hybrid_power.alphabet_disc:
		alphabet.append(letter)

	for letter in hybrid_power.alphabet_cont:	
	#for letter in hybrid_power.alphabet_cont:
	#	alphabet.append(letter)
		alphabet.append(letter)

	matrices = dict()
	for letter in alphabet:
		if letter in hybrid_power.alphabet_cont:
			matrices[letter] = \
			   hybrid_power.ComputeBigAmatrix(\
			    state_index, dstate_index,\
			    letter, state_dimension )
			  
		elif letter in hybrid_power.alphabet_disc:
			matrices[letter] = \
			  hybrid_power.ComputeBigResetMap(\
		            state_index, dstate_index,\
                                        letter, state_dimension)
	
	
			       
		else:
			print "Something is rotten in \
			       Denmark\n"
	
	
	output = zeros ((hybrid_power.output_dimension, \
	                 state_dimension), 'd')
	
	for (key, value) in state_index.iteritems():
		output[0:hybrid_power.output_dimension, \
		       value[0]:value[1]] = \
		 hybrid_power.c_matrices[key]
	
	zeta = dict()

	repr_index = []
	for index in hybrid_power.index_first:
		zeta[index] = zeros((state_dimension,1), \
		                     'd' )
		
		(dstate, cstate) = hybrid_power.zeta[index]

		irange = state_index[dstate]

		#print "Range: " + str(irange)+\
		#	"zeta: "+\
		#	str(zeta[index][irange[0]:\
		#	                irange[1],0])\
		#	+ " cstate : "+\
		#	str(cstate)+"\n"
		zeta[index][irange[0]:irange[1],0] = \
		reshape(cstate, (hybrid_power.state_dimensions[dstate],))
		repr_index.append(index)
	
	#for (key, value) in dstate_index.iteritems():
		for sindex  in hybrid_power.index_second:
			zeta[(index,sindex)] =\
		             zeros ((state_dimension,1),\
		                                   'd' )
	 	
			value = dstate_index[\
			          (dstate, sindex)]
			zeta[(index, sindex)][\
			             value[0],0] = 1 
			
			repr_index.append((index,sindex))

	input_data = dict()

	input_data["alphabet"] = alphabet
	input_data["matrices"] = matrices
	input_data["output"]   = output
	input_data["zeta"]     = zeta
	
	input_data["state_index"]  = state_index
	input_data["dstate_index"] = dstate_index
	input_data["b_matrices"]   = hybrid_power.b_matrices
	input_data["index_second"] = \
	                     hybrid_power.index_second
	input_data["alphabet_cont"] = \
	                     hybrid_power.alphabet_cont
	input_data["discrete_states"]  = \
	                     hybrid_power.discrete_states
	
	input_data["state_dimensions"] =\
	                     hybrid_power.state_dimensions
	
	input_data["repr_index"] = repr_index

#		                  alphabet, matrices, \
#		                  output, zeta,\
#				  state_index, dstate_index,\
#				  self.b_matrices, \
#				  self.index_second, \
#				  self.alphabet_cont,\
#				  self.discrete_states, \
#				  self.state_dimensions )
	
	return input_data
		
class RepresentationForHybridPower(Representation):
	attributes = ["alphabet", "transition",\
	              "output", "zeta",\
		       "state_index",\
		      "index_second", "index_first",\
		      "dstate_index",  "alphabet_cont",\
		      "discrete_states", "state_dimensions",\
		      "repr_index"]
	class_attrib = [\
		       "state_index",\
		      "index_second",\
		      "dstate_index",  "alphabet_cont",\
		      "discrete_states", "state_dimensions",\
		      "repr_index"]
		
	def __init__(self, **arguments):
	            #alphabet, transition, output, zeta,\
	            # state_index, dstate_index, b_matrices,\
		    # index_second, alphabet_cont, 
		    # discrete_states, state_dimensions ):
		if arguments.has_key("repr") and \
		   arguments.has_key("repr_hyb"):
			Representation.__init__(\
			self,[],[],[],[],\
			copy = arguments["repr"]\
			)
			for field in \
			 RepresentationForHybridPower.class_attrib:
			 	self.__dict__[field] = \
				arguments["repr_hyb"].__dict__[field]
				
			return	

		if arguments.has_key("hyb_pow"):
			input_data = \
			 ReprFromHybridPower( arguments["hyb_pow"])
		else:
			input_data = arguments
			
		self.repr_index      = input_data["repr_index"]	
		self.state_index     = input_data["state_index"] 
		self.dstate_index    = input_data["dstate_index"]
		self.index_second    = input_data["index_second"]
		self.alphabet_cont   = input_data["alphabet_cont"]
		self.discrete_states = input_data["discrete_states"]
		self.state_dimensions = \
		           input_data["state_dimensions"]
		
		Representation.__init__(self,\
		        input_data["alphabet"],\
		        input_data["matrices"], \
			input_data["output"], input_data["zeta"])
		
		#self.b_matrices = dict()
		#for (key, value) in \
		#      input_data["b_matrices"].iteritems():
		#	self.b_matrices[key] =\
		#	      zeros((self.dimension,1),'d')
		#	
		#	srange = self.state_index[key[0]]
		#	self.b_matrices[key][\
		#	                srange[0]:srange[1]] = \
		#			value
		
	#def MinimalRepresentation(self):
	#	if self.__dict__.has_key("mrepr"):
	#		return self.mrepr
#
#		mrepr = Representation.MinimalRepresentation(self)
#
#		self.__dict__["mrepr"]=mrepr
#		
#		#mrepr_hyb = RepresentationForHybridPower(\
#		#                 repr = mrepr, repr_hyb = self)
#		
#		return mrepr

	def HankelMatrix(self, length=[], size=[], \
	                                  index_list = []):
		if index_list == []:
			index_list = self.repr_index
		
		print "Index list"+str(index_list)+"\n"
		return Representation.HankelMatrix(self, length,\
		       size, index_list)

	def ownprint(self):
		Representation.ownprint(self)

		print "State_index:"+str(self.state_index)+\
		      "Discrete_state_index:" + \
		      str(self.dstate_index)+\
		      "Alphabet_cont: "+ str(self.alphabet_cont)\
		      + "index_second: " + str(self.index_second)\
		      +"\n"
		      #+ "b_matrices: "+str(self.b_matrices)\

	def NewComputeContOutput(self,state_list):
		
		index_list = self.index_second

		if index_list==[]:
			return array([[0]],'d')

		new_zeta=dict([])

		for state in state_list:
			for index in index_list:
				new_zeta[(state,index)] = zeros(\
			           (self.dimension,1), 'd')

				new_zeta[(state,index)][\
			     		self.dstate_index[(state, index)][0]]=1

		new_rep=Representation(alphabet=self.alphabet,transition=self.transition,output=self.output,zeta=new_zeta)
		
		mrepr=new_rep.MinimalRepresentation()

		cont_outputs=dict([])

		for state in state_list:
			output_list=[]
			for index in index_list:
				init_states=[]
				for jindex in range(mrepr.dimension):
					init_states.append(mrepr.zeta[(state,index)][jindex])

				print "Init states:"+str(init_states)+"\n"
				output_list.append(init_states)

			print "Output list:"+str(output_list)+"\n"
					
			output_matrix = array(output_list, 'd')	
			#output_matrix = transpose(output_matrix)
			cont_outputs[state]=output_matrix

			print "Len: "+str(len(output_matrix))+"\n"


		#for state in state_list:
		#	for index in index_list:
		#		self.zeta.pop((state,index))
				

		return cont_outputs
			

	def ComputeContOutput(self, state, depth):
		index_list = self.index_second

		if index_list==[]:
			return array([[0]],'d')



		for index in index_list:
			self.zeta[index] = zeros(\
			           (self.dimension,1), 'd')

			self.zeta[index][\
			     self.dstate_index[(state, index)][0]]\
			                                     = 1
			
		if depth == []:
			(hmatrix, hsize, hindex) = self.HankelMatrix([],[], index_list)
			depth = hsize

		hankel_table_gen = self.HankelTableGen(index_list)
		

		disc_output = hankel_table_gen.ComputeMarkov(\
		                             depth, index_list)

		for index in index_list:
			self.zeta.pop(index)
			
		disc_output_matrix = array(disc_output, 'd')	
		disc_output_matrix = transpose(disc_output_matrix)
		return disc_output_matrix
		
		
	def DiscContObsMatrix(self, state):
		own_print(False, "DiscObs, state"+str(state)+"\n")
		obs_matrix = self.ObservabilityMatrix()

		obs_matrix = transpose(obs_matrix)

		rrange = self.state_index[state]
		
		own_print(False, "ObsMatrix:"+array2string(obs_matrix)+" rrange:"+str(rrange)+"\n")
		own_print(False, "ObsMatrix1:"+array2string(obs_matrix[0:6,2:4])+"\n")

		obs_state = obs_matrix[0:obs_matrix.shape[0],\
		                       rrange[0]:rrange[1]]

		own_print(False, "State: "+str(state)+\
		                "Osb_state:"+array2string(obs_state)+"\n")
		return obs_state

	
	def IsDiscStateObservable(self ):
		for state in self.discrete_states:
			obs_state_matrix = self.DiscContObsMatrix(\
			                      state )
			
			orank = compute_rank( obs_state_matrix )

			if not orank[0] == \
			   self.state_dimensions[state]:
			   	return False
				
		return True
	

def ComputeInverseTransition( automata ):
	inverse_transition = dict()

	for state in automata.state:
		inverse_transition[state]=[]
	
	for (key, value) in automata.transition.iteritems():
		print "Key, "+str(key)+"value: "+str(value)+"\n"
		inverse_transition[value].append((key[0], key[1]))
		print "Inverse_trans: "+\
		       str(inverse_transition[value])+"\n"
		
	return inverse_transition

def ComputePath( automata, state, inverse_transition = [],\
                 visited_states=[]):
	
	if visited_states == []:
		visited_states = Set()
	else:
		if state in visited_states:
			return ([],[])
			
	
	visited_states.add( state )
	
	
	for (key, value) in automata.zeta.iteritems():
		if value == state :
			return (key, [])
			
	if inverse_transition == []:
		inverse_transition = \
		    ComputeInverseTransition( automata )

	for back in inverse_transition[state]:
		(initial_state, path) \
		      = ComputePath( automata, back[1], \
				       inverse_transition,\
				       visited_states )
		if not initial_state == []:
			path.append(back[0])
			return (initial_state, path)
	
	return ([],[])
	

class PreHybridPower:
	InputAttributes =[ "repr", "automata", "index_first", \
	                    "index_second", "alphabet_disc",\
			    "alphabet_cont" ]
	def __init__ (self, **arguments):
		
		for attrib in PreHybridPower.InputAttributes:
			self.__dict__[attrib] = \
			   arguments[attrib]
		#self.repr = repr
		#self.automata = automata
		#self.index_first = index_first
		#self.index_second = index_second
		#self.alphabet_disc = alphabet_disc
		#self.alphabet_cont = alphabet_cont
		
	
	def ComputeAmatrix( self, state, letter, \
	                         state_spaces ):
				 
		matrix = matrixmultiply(\
		  transpose(state_spaces[state]),\
		  matrixmultiply( \
			 self.repr.transition[letter],\
		          state_spaces[state])\
		  )

  		return matrix
	
	def ComputeBmatrix( self, state, letter, \
	                      state_spaces, sindex, \
			      inverse_transition ):
			      
		(index, w) = ComputePath(self.automata, state)
		matrix     = self.repr.ComputeMatrixProduct(w)	
		matrix 	   = matrixmultiply( \
		              self.repr.transition[letter],\
			      matrix )

		matrix     = matrixmultiply( matrix, \
		               self.repr.zeta[(index,sindex)] )
		
		matrix     = matrixmultiply( \
		               transpose(state_spaces[state]),\
			       matrix)
		
		return matrix

	def ComputeResetMap (self, state, letter, state_spaces ):	
		  
		dstate = self.automata.transition[(letter, state)]
					    
		matrix = matrixmultiply(\
		   transpose(state_spaces[dstate]),\
			matrixmultiply(\
			 self.repr.transition[letter], \
			 state_spaces[state])\
		   )
		 
		return matrix
				 
	def ComputeCmatrix (self, state, state_space ):		
		matrix = matrixmultiply(\
			    self.repr.output, state_space[state])

		return matrix
	

	def ComputeHybridPower( self ):
		inverse_transition = ComputeInverseTransition( \
		                           self.automata )
		state_spaces = self.ComputeStateSpace(\
		                         inverse_transition)
	 	
		automata = self.automata.copy()

		for state in automata.state:
			automata.output[state] = \
			 self.automata.output[state].discrete_output

		a_matrices = dict()
		b_matrices = dict()
		c_matrices = dict()
		reset_maps = dict()
		
		for state in automata.state:
			for letter in self.alphabet_cont:
				a_matrices[(state,letter)] = self.\
				  ComputeAmatrix(state, letter,\
				                state_spaces )

				for sindex in self.index_second:
					b_matrices[(state,\
					 (letter, sindex))] =\
					  self.ComputeBmatrix( \
					   state,letter, \
					   state_spaces, sindex,\
					   inverse_transition )
				
			for letter in self.alphabet_disc:
				reset_maps[(letter, state)]=\
				 self.ComputeResetMap(\
				  state, letter,state_spaces )
			
			c_matrices[state] = \
			  self.ComputeCmatrix( state,state_spaces)
		
		zeta = dict()
		for index in self.index_first:
			zeta[index]=(\
			  self.automata.zeta[index], \
			  matrixmultiply(\
			   transpose(\
			    state_spaces[\
			     self.automata.zeta[index]]),\
			    self.repr.zeta[index]))
		
		hybpow = hybrid_power.HybridPower(automata = automata, \
		     alphabet_disc = self.alphabet_disc,\
		     alphabet_cont = self.alphabet_cont, \
	             index_first = self.index_first, \
	             index_second = self.index_second, \
		     a_matrices = a_matrices,\
		     b_matrices = b_matrices, \
		     c_matrices = c_matrices,\
		     reset_maps = reset_maps, zeta = zeta )
		
		return hybpow
							     


	def ComputeInitialStateSpace(self, inverse_transition):	
					 
		ReachDiscState = dict()			 
		for state in self.automata.state:
			ReachDiscState[state] = zeros(\
			            (self.repr.dimension,1),'d')	
			(index,w) = ComputePath(self.automata,\
			                    state,\
				            inverse_transition)
			print  "state: "+str(state)+\
			       "  index: "+str(index)+\
			       "  w "+ str(w)+"\n"
		        rmatrix  = self.repr.ComputeMatrixProduct(\
			                      w)

			is_empty = True
			
			for sindex in self.index_second:
				for letter in \
				        self.alphabet_cont:
					smatrix = \
					matrixmultiply(\
					  self.repr.transition[\
					  letter], \
					   rmatrix)

					rside = matrixmultiply(\
					      smatrix,\
			 	              self.repr.zeta[\
				              (index, sindex)])
					if is_empty:
					  	matrix = rside
						is_empty = False
					else:
					 	matrix = \
						 concatenate(\
						  (matrix, \
						   rside ), 1)
					      
			if not is_empty:	
				ReachDiscState[state] = matrix
				    
		for index in self.index_first:
			dstate = self.automata.zeta[index]
			matrix = reshape(self.repr.zeta[index],\
		                    (self.repr.dimension,1))
			if is_empty:	    
				ReachDiscState[dstate] = \
				        matrix
				is_empty = False		
			else:		
				ReachDiscState[dstate]=\
				 concatenate(\
			               (matrix,\
				       ReachDiscState[dstate]), 1)
		
		print "Init:\n"+str(ReachDiscState)+"\n"
		return ReachDiscState
		
	def ComputeStateSpace( self, inverse_transition ):
		       
		#inverse_transition = ComputeInverseTransition( \
		#                     self.automata )
		
		graph=dict()
		for state in self.automata.state:
			graph[state]=[]
			for element in inverse_transition[state]:
				graph[state].append(element)
				
			for letter in self.alphabet_cont:
				graph[state].append((letter, state))

		ReachDiscState = self.ComputeInitialStateSpace(\
		                  inverse_transition )
		#for state in self.automata.state:
		#	ReachDiscState[state] = zeros(\
		#	            (self.repr.dimension,1),'d')	
				    
		#for index in self.index_first:
		#	dstate = self.automata.zeta[index]
		#	matrix = reshape(self.repr.zeta[index],\
		#                    (self.repr.dimension,1))
		#
		#	for sindex in self.index_second:
		#		matrix = concatenate( (matrix, \
		#	 	          self.repr.zeta[\
		#		          (index, sindex)]), 1)
		#	
		#	ReachDiscState[dstate] = matrix
		
		state_space = base_compute_recursive( \
		                        ReachDiscState,\
	                                self.repr.transition, \
					graph )
					      
		return state_space 		


def ComputeReprLists( repr_index, repr_alphabet, index_first,\
                       index_second, alphabet_disc, \
		       alphabet_cont):
		       	       
	for index in index_first:
		repr_index.append(index)
		for sindex in index_second:
			repr_index.append(\
			(index, sindex))
		
	for letter in alphabet_disc:
		repr_alphabet.append(letter)
	
	for letter in alphabet_cont:
		repr_alphabet.append(letter)
		
	return

class HybridHankel:
	input_attributes = ["hankel_matrix", "hankel_table",\
	                    "index_first", "index_second",\
			    "alphabet_disc", "alphabet_cont",
			    "cont_output_dimension",\
			    "hankel_matrix_length", \
			    "hankel_table_length"]

			    
	attributes =    [ "hankel_matrix", "hankel_table",\
	                    "index_first", "index_second",\
			    "alphabet_disc", "alphabet_cont",\
			    "cont_output_dimension",\
			    "hankel_matrix_length", \
			    "hankel_table_length", \
			    "repr_index", "repr_alphabet",\
			    "hankel_table_obj"
			    ]
	def initialize(self):
		self.repr_index = []
		self.repr_alphabet = []
		
		ComputeReprLists(self.repr_index, \
		   self.repr_alphabet,\
		   self.index_first, self.index_second,\
		   self.alphabet_disc, self.alphabet_cont)

		
	def copy(self, other):
		for field in HybridHankel.attributes:
			self.__dict__[field] = \
			  other.__dict__[field]
		
		return

			
	def __init__(self, **arguments):
		if arguments.has_key("hyb_hankel"):
			HybridHankel.copy(self, \
			    arguments["hyb_hankel"])
			return
			
		input_data = arguments
		if arguments.has_key("ext_hankel_table_obj"):
			self.hankel_table_obj = \
			      arguments["ext_hankel_table_obj"]
			input_data["hankel_table"] = \
			    self.hankel_table_obj.table      
			 
			input_data["hankel_table_length"] = \
			   self.hankel_table_obj.depth
			   
		elif arguments.has_key("hankel_table_obj"):
			input_data["hankel_table"] =\
			       input_data["hankel_table_obj"].\
			           table
			
			input_data["hankel_table_length"] =\
			      input_data["hankel_table_obj"].depth
			                  
		for field in HybridHankel.input_attributes:
			self.__dict__[field] = input_data[field]
		
		HybridHankel.initialize(self)
	

	def ownprint(self):
		for attrib in HybridHankel.attributes:
			print "Attrib:"+str(attrib)+"\n"
			if attrib == "hankel_matrix":
				print array2string(self.hankel_matrix, precision=2, suppress_small = 1)+"\n"	
			elif attrib == "hankel_table_obj":
				self.hankel_table_obj.ownprint()
			elif attrib == "hankel_table":
				if not \
				  self.__dict__.has_key(\
				  "hankel_table_obj"):
					print str(\
					  self.hankel_table)+"\n"
			
			else:
			   print str(self.__dict__[attrib])+"\n"

			   
	def GetHankelMatrixObject(self):
		if not self.__dict__.has_key("hankel_matrix_obj"):
			self.hankel_matrix_obj = \
			HankelMatrix(self.repr_alphabet, \
			      self.cont_output_dimension, \
			      self.hankel_matrix,\
			      self.repr_index, self.hankel_matrix_length)
		
		return self.hankel_matrix_obj
		
	
#	def ComputeExtendColumns(self, ext_hankel_table,\
#	                           wcont_hankel_col,\
#				   wdisc_hankel_col, size):
#	if size = 0:			   
#		htcol_index = (wdisc_hankel_col +1)*\
#		               len(self.index_first)  
#		hccol_index = (wcont_hankel_col+1) *\
#		              len(self.repr_index)
#		for findex in self.index_first:
#			ccolumn = []
#			for sindex in self.index_second:
#				ccolumn.append(\
#				  self.hankel_matrix[:,\
#				   hccol_index])
#				hccol_index = hccol_index + 1	   
#			ext_hankel_table.append((\
#			          self.hankel_table(\
#				    thcol_index, hccol_index))
#			
#			htcol_index = htcol_index + 1
#	else:
#		for nletter in range(len(self.alphabet_disc)):
#			wcont_hankel_col = (wcont_hankel_col+1)*\
#			          len(self.repr_alphabet)+1
#			
#			wdisc_hankel_col = (wdisc_hankel_col+1)*\
#			          len(self.alphabet_disc) + 1
#				  
#			self.ComputeExtColumns(ext_hankel_table,\
#			    wcont_hankel_col, \
#			    wdisc_hankel_col, size - 1)
#				   
#	
#	 return
	 
	
	def composition_operator(self, func1, func2):
		size        = func2[2] + 1
		if size < self.hankel_table_length:
			col_table_index = (func2[0][0]+1)\
			         *len(self.alphabet_disc)\
		                   + func1[1]
			row_table_index = -1
		else:
			col_table_index = func2[0][0]
			row_table_index = (func2[0][1]+1)*\
			        len(self.alphabet_disc)+ func1[1]
				   
		table_index = (col_table_index, row_table_index)
		
		hankel_index = (func2[1]+1)*\
		                  len(self.repr_alphabet)+func1[1]
		return (table_index, hankel_index, size)				  
	def output_function(self, func, index,oindex):
		col_table  = (func[0][0]+1)*len(self.index_first)
		row_table  = (func[0][1]+1)
		col_hankel = (func[1] +1 ) * len(self.repr_index)
	
		cont_output = []
		for sindex in range(len(self.index_second)):
			cont_column = self.hankel_matrix[:,\
				    col_hankel+index+sindex]

			cont_output.append(cont_column)
		
		disc_output = self.hankel_table[col_table][\
		                                   row_table]
		
		
		disc_obj = ContDiscreteOutput(\
		               cont_output, disc_output)

		return disc_obj
	
	def size_function(self, table):
		return len(table)

	def ComputeExtendedHankelTable(self):
	 	ext_hankel_table_pair = []

		transition_function=dict()

		alphabet = range(len(self.alphabet_disc))
		
		for ind in alphabet:
			transition_function[ind] =\
			((ind,-1), ind, 1)

		identity_function = ((-1,-1), -1, 0)
		

		abs_hank = HankelTableGenerator(\
		    transition_function = transition_function,\
		    output_function = self.output_function, \
		    composition_operator = \
		               self.composition_operator, \
		    alphabet = alphabet,\
		    size_function = self.size_function,\
		    identity_map = identity_function,\
		    index_set = range(len(self.index_first)),\
		    output_range = [1] )
		

		(ext_hankel_table_pair, tsize) = \
		               abs_hank.HankelTable(\
			       self.hankel_table_length,\
			       len(self.hankel_table))
		
 		return ext_hankel_table_pair
		    
		
	def GetHankelExtTableObject(self):
		if not self.__dict__.has_key("hankel_table_obj"):
			ext_hankel_table = \
			    self.ComputeExtendedHankelTable()
			self.hankel_table_obj = \
			 HankelTable(\
			   alphabet  = \
			                self.alphabet_disc, \
			   table     = ext_hankel_table,\
			   index_set = self.index_first,\
			   depth     = self.hankel_table_length)
		
		return self.hankel_table_obj
		
		
	def ComputeHybridPowerRepr(self):
		hankm = self.GetHankelMatrixObject()		
		hank_table = self.GetHankelExtTableObject()

		repr = hankm.ComputeRepresentation()

		print "Representation from Hankel matrix\n"
		repr.ownprint()

		auto = hank_table.ComputeMooreAutomata()

		print "Automata\n"
	        auto.ownprint()

		pre_hyb = PreHybridPower(\
		             automata        = auto,\
			     repr            = repr, \
			     index_first     = self.index_first,\
			     index_second    = self.index_second,\
			     alphabet_disc   = self.alphabet_disc,\
			     alphabet_cont   = self.alphabet_cont)

		hyb_pow  = pre_hyb.ComputeHybridPower()

		return hyb_pow

	def __eq__  (self, other):
		Epsilon = 1e-5

		is_equal = \
		 (self.alphabet_disc == other.alphabet_disc) and\
		 (self.alphabet_cont == other.alphabet_cont) and\
		 (self.index_first   == other.index_first  ) and\
		 (self.index_second  == other.index_second ) and\
		 (self.hankel_matrix_length == \
		       other.hankel_matrix_length ) and\
		 (self.hankel_table_length == \
		              other.hankel_table_length )  and\
		 ((self.hankel_matrix - other.hankel_matrix) < \
		  Epsilon ).all() and \
		 (self.hankel_table == other.hankel_table)
		
		own_print(True, " Alphabet_disc:"+
		 str(self.alphabet_disc == other.alphabet_disc) +\
		 " Alpahebt_cont:"+\
		 str(self.alphabet_cont == other.alphabet_cont) +\
		 " Index first : "+\
		 str(self.index_first   == other.index_first  ) +\
		 " Index_second "+\
		 str(self.index_second  == other.index_second ) +\
		 " Hanke matrix length: "+\
		 str(self.hankel_matrix_length == \
		       other.hankel_matrix_length ) +\
		  " Hankel table length: "+\
		 str(self.hankel_table_length == \
		              other.hankel_table_length )  +\
		" Hankel matrix diff: "+\
		 array2string(self.hankel_matrix - other.hankel_matrix,\
		   precision =3, suppress_small = 1) + \
		  " Hankel matrix eps. diff: "+\
		  str(\
		 (((self.hankel_matrix - other.hankel_matrix) < \
		  Epsilon ).all()) )+\
		  " Hankel table diff: "+\
		 str(self.hankel_table == other.hankel_table)+"\n")

		return is_equal
		       
		 
		                  
