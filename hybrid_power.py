from  form_pow_repr import *
#from dfa_automata import *
from own_io import own_print
from  numpy import *
from word_operations import *
from auxiliary_hybrid import *
import ConfigParser


class HybridPower:
	attribute_list= \
	 	["index_first", "index_second",\
	                "automata", "alphabet_disc", \
			"alphabet_cont", "reset_maps",\
			"a_matrices", "b_matrices",\
			"c_matrices", "zeta", "discrete_states",\
			"state_dimensions", "output_dimension"]
	
	matrix_attributes = ["a_matrices", "b_matrices", \
	                     "c_matrices", "reset_maps" ]
	
	input_attribute_list =\
	 	["index_first", "index_second",\
	                "automata", "alphabet_disc", \
			"alphabet_cont", "reset_maps",\
			"a_matrices", "b_matrices",\
			"c_matrices", "zeta" ]

			#"state_dimensions", "output_dimension"]
	def copy(self, other):
		for attrib in HybridPower.attribute_list:
			self.__dict__[attrib] = \
				other.__dict__[attrib]
		
		return
	
	def __init__(self, **arguments):
		if arguments.has_key("hyb_pow"):
			HybridPower.copy(self, \
			        arguments["hyb_pow"])
			
			return
			
		input_data = dict()	
		if arguments.has_key("config_file"):
			fp = arguments["config_file"]
			configf = ConfigParser.ConfigParser()

			configf.readfp(fp)
			
			input_data_list = configf.items(\
			                 "HybridPower")
			for element in input_data_list:
				key   = element[0]
				value = element[1]
				input_data[key] = \
				  eval(value)
			
			fp.seek(0)
			input_data["automata"] = MooreAutomata(\
				config_file = fp)
				
		elif arguments.has_key("automata_config_file"):
			input_data = arguments

			input_data["automata"] = MooreAutomata(\
			    config_file = \
			     arguments["automata_config_file"])
			
		else:	
			input_data = arguments			
			
		for attrib in HybridPower.input_attribute_list: 
			if input_data.has_key(attrib):
				self.__dict__[attrib]=\
				 input_data[attrib]	
			else:
			 	print "Missing attribute :"+\
				   str(attrib)+ "\n"
				return
		
		self.state_dimensions = dict()

		self.discrete_states = []
		for state in self.automata.state:
			self.discrete_states.append(state)

		for state in self.discrete_states:
			self.state_dimensions[state] =\
			  self.c_matrices[state].shape[1]
		
		self.output_dimension = self.c_matrices[self.discrete_states[0]].shape[0]

	def GetDiscreteStateSize(self):
		return len(self.automata.state)
	
	def GetContStateSize(self):
		cont_state_size = 0
		for (key, value) in \
		        self.state_dimensions.iteritems():
			cont_state_size = cont_state_size + value
		cont_state_size = cont_state_size + \
		      (len(self.index_second) *\
		          len(self.automata.state))
		return cont_state_size 
		
	def GetMinimalRepresentation(self):
		if not self.__dict__.has_key("minimal_repr"):
			repr  = self.GetRepresentation()
			mrepr = repr.MinimalRepresentation()
			self.minimal_repr = mrepr
		
		return self.minimal_repr
	
	def GetContHankelMatrix(self,hsize=[], hstop_rank = []):
		if not self.__dict__.has_key("hankel_matrix"):
			mrepr = self.GetMinimalRepresentation()
			if hsize == []:
		        	hsize = mrepr.dimension
			
			if hstop_rank == []:
				hstop_rank = mrepr.dimension
		
			repr = self.GetRepresentation()
			
			print "Mrepr:hankel\n"
			print "Hsize: "+str(hsize)+" hstop:"+str(hstop_rank)+\
                               "index: "+str(repr.repr_index)+"\n"

			(hankm, hsize_ret, hindex_list) =\
			     mrepr.HankelMatrix(hsize, hstop_rank,\
			       repr.repr_index)
			#print "Repr:hankel\n"
			#(hankm2, hsize_ret2, hindex_list2) = \
			#    repr.HankelMatrix(hsize, hstop_rank)
			#
			#own_print(True, "Diff. : "+\
			#      array2string(hankm2-hankm, precision=2,\
			#           suppress_small = 1)+"\n")
			#own_print(True, "Diff1:"+str(hsize_ret == hsize_ret2)+"\n")
			#own_print(True, "Diff2:"+str(hindex_list == hindex_list2)+"\n")

			self.hankel_matrix      = hankm
			self.hankel_matrix_size = hsize_ret
			self.hankel_matrix_indl = hindex_list
		
		return \
		 (self.hankel_matrix, self.hankel_matrix_size,\
		       self.hankel_matrix_indl)
		
			
	def ownprint(self, oprecision=2, sup_smal=1):
		for attrib in HybridPower.attribute_list:
			print str(attrib)+": "
			if attrib in HybridPower.matrix_attributes:
				for (key,value) in \
				 self.__dict__[attrib].iteritems():
				
					print "Key: "+ str(key)+\
					   "\n "+\
					   array2string(\
				            value,\
					    precision=oprecision, \
				            suppress_small=\
					    sup_smal)+"\n"
			elif attrib == "zeta":
				for (key, value) in \
				  self.zeta.iteritems():
				  	print "Key: "+\
					      str(key)+\
					      " discrete_mode: "+\
					      str(value[0])+\
					      " vector:"+\
					      array2string(\
					      value[1],precision \
					      = oprecision, \
					      suppress_small =\
					      sup_smal)+"\n"
			elif attrib == "automata":
				print "Automata\n"
				self.automata.ownprint()
			else:	
				print str(self.__dict__[attrib])
			
	def ComputeBigResetMap(self, state_index, dstate_index,\
				letter, state_dimension):
		
		matrix = zeros((state_dimension, \
		               state_dimension), 'd' )
			       
			       
		for (key, value) in state_index.iteritems():
			 #"Reset_maps:"+\
			 #     str(self.reset_maps[(letter,key)])+\
			 #     " right-hand side "+\
			 #     str(matrix[rvalue[0]:rvalue[1],\
			 #         value[0]:value[1]])+\
			#print "key: "+str(key)+\
			#	   " letter "+ str(letter)+"\n"
			rkey = self.automata.transition[\
			 (letter, key)]

			rvalue = state_index[rkey]
					 
			#print "Reset_maps:"+\
			#      str(self.reset_maps[(letter,key)])+\
			#      " right-hand side "+\
			#      str(matrix[rvalue[0]:rvalue[1],\
			#           value[0]:value[1]])+\
			#	   "key: "+str(key)+\
			#	   " letter "+ str(letter)+\
			#	   "rkey: "+ str(rkey)+"\n"
			matrix[\
			   rvalue[0]:rvalue[1],\
			   value[0]:value[1] ] = \
			 self.reset_maps[ (letter,key)]
			 
		for (key,value) in dstate_index.iteritems():
			#print "Key[0]: "+str(key[0])+\
			#   "key[1]: "+str(key[1])+\
			#   "value: "+str(value)+\
			#   "letter:"+str(letter)+\
			#   "transition:"+ str(self.automata.transition[(letter, key[0])])+\
			#   "\n"
			rdstate = self.automata.transition[\
			   (letter,key[0])]
			
			rvalue = dstate_index[(rdstate,\
				    key[1])]
			
			matrix[rvalue[0],value[0]] = 1
			
		return matrix
	def ComputeBigAmatrix(self, state_index,\
	                       dstate_index, letter,\
				state_dimension ):
	
		matrix = zeros((state_dimension, state_dimension),\
		                'd' )

		for key in self.discrete_states:
			value = state_index[key]
	   		matrix[\
			 	 value[0]: value[1], \
				 value[0]: value[1] \
			       ] =\
			  self.a_matrices[(key, letter)]
				
		for (key, value) in dstate_index.iteritems():
			dstate = key[0]
			index  = key[1]
			rvalue = state_index[dstate]
			
			matrix[ rvalue[0]:rvalue[1], \
			        value[0] ] = reshape(\
			self.b_matrices[(dstate,(letter, index))],\
			(self.state_dimensions[dstate],))
			 
		
		return matrix 

		
	def ComputeRepresentation(self):
		repr = RepresentationForHybridPower( \
		                          hyb_pow = self )
		return repr

#		state_dimension = 0
#
#		state_index = dict()
#		for (key,value) in self.state_dimensions.iteritems():
#			state_index[key] = (state_dimension,\
#				state_dimension + value )
#	  		state_dimension = state_dimension + value
#	
#		dstate_index = dict()
#		for index in self.index_second:
#			for dstate in self.discrete_states:
#				dstate_index[(dstate, index)] = (\
#				     state_dimension, \
#				     state_dimension + 1 )
#				state_dimension = state_dimension+1
#			
#		alphabet =[]
#		for letter in self.alphabet_disc:
#			alphabet.append(letter)
#
#		for letter in self.alphabet_cont:	
#		#for letter in self.alphabet_cont:
#		#	alphabet.append(letter)
#			alphabet.append(letter)
#
#		matrices = dict()
#		for letter in alphabet:
#			if letter in self.alphabet_cont:
#				matrices[letter] = \
#				   self.ComputeBigAmatrix(\
#				    state_index, dstate_index,\
#				    letter, state_dimension )
#				  
#			elif letter in self.alphabet_disc:
#				matrices[letter] = \
#				  self.ComputeBigResetMap(\
#			            state_index, dstate_index,\
#                                        letter, state_dimension)
#		
#		
#				       
#			else:
#				print "Something is rotten in \
#				       Denmark\n"
#		
#		
#		output = zeros ((self.output_dimension, \
#		                 state_dimension), 'd')
#		
#		for (key, value) in state_index.iteritems():
#			output[0:self.output_dimension, \
#			       value[0]:value[1]] = \
#			 self.c_matrices[key]
#		
#		zeta = dict()
#
#		for index in self.index_first:
#			zeta[index] = zeros((state_dimension,1), \
#			                     'd' )
#			
#			(dstate, cstate) = self.zeta[index]
#
#			irange = state_index[dstate]
#
#			#print "Range: " + str(irange)+\
#			#	"zeta: "+\
#			#	str(zeta[index][irange[0]:\
#			#	                irange[1],0])\
#			#	+ " cstate : "+\
#			#	str(cstate)+"\n"
#			zeta[index][irange[0]:irange[1],0] = \
#			reshape(cstate, (self.state_dimensions[dstate],))
#		
#		#for (key, value) in dstate_index.iteritems():
#			for sindex  in self.index_second:
#				zeta[(index,sindex)] =\
#			             zeros ((state_dimension,1),\
#			                                   'd' )
#		 	
#				value = dstate_index[\
#				          (dstate, sindex)]
#				zeta[(index, sindex)][\
#				             value[0],0] = 1 
#	
#		repr = RepresentationForHybridPower( \
#		                  alphabet, matrices, \
#		                  output, zeta,\
#				  state_index, dstate_index,\
#				  self.b_matrices, \
#				  self.index_second, \
#				  self.alphabet_cont,\
#				  self.discrete_states, \
#				  self.state_dimensions )
#		
#		return repr

	def GetRepresentation(self):
		if self.__dict__.has_key("repr"):
			return self.repr
		else:
			self.repr = self.ComputeRepresentation()
		
		return self.repr
		

	def ComputeAutomaton(self, **arguments):
	       #**arguments):
		
		repr = self.GetRepresentation()

		#own_print(True, "Getting minimal representation\n")
		#mrepr = self.GetMinimalRepresentation()
	
		#own_print(True, "Computing the depth of \
		                            #the cont.out\n")	
		if arguments.has_key("depth"):
			depth = arguments["depth"]
		else:
			#(hankm, hsize, lindex) = \
			#    self.GetContHankelMatrix()
			depth = repr.dimension 
			#depth=[]
			
			
			#mrepr.dimension
			
		own_print(True, "Depth of the cont. output:"+\
		                 str(depth)+"\n")
		                 
		
		#alphabet = self.alphabet_disc
		#alphabet.extend(self.alphabet_cont)
		automata = self.automata.copy()

		#automata.ownprint()

		automata.output = dict()

		#repr = self.GetRepresentation()
		coutput_dict = repr.NewComputeContOutput(automata.state)
		for state in automata.state:
		#	coutput = repr.ComputeContOutput(state,\
		#	               depth)
			coutput=coutput_dict[state]
			own_print(False, "Coutp\n"+str(coutput)+"\n")
			automata.output[state]=ContDiscreteOutput(\
			     coutput, \
			     self.automata.output[state])
			automata.output[state].ownprint()
		
		return automata
		

	def GetExtendedAutomata(self):
		if not self.__dict__.has_key("ext_automata"):
			self.ext_automata =\
				self.ComputeAutomaton()
		
		return self.ext_automata
		
	def IsObservable(self):
		repr = self.GetRepresentation()
		
		automata = self.GetExtendedAutomata()
		 
		if repr.IsDiscStateObservable() and \
		   automata.IsObservable():
		   	return True
		
		return False
	
	def IsReachable(self):
		repr = self.GetRepresentation()
		automata = self.GetExtendedAutomata()

		return ( repr.IsReachable() and \
		         automata.IsReachable())
	

	def ComputeMinimal( self ):
		own_print(True, "Computing representation\n")
		#repr = self.GetRepresentation()
		own_print(True, "Computing automaton\n")
		automata = self.GetExtendedAutomata()

		mrepr = self.GetMinimalRepresentation()
		print "Minimal representation in ComputeMinimal\n"
		mrepr.ownprint()
		mautomata = automata.MinimalAutomaton()

		premhyb = PreHybridPower( repr = mrepr,\
		                          automata = mautomata,\
					  index_first = \
					   self.index_first, \
					  index_second = \
					    self.index_second, \
					  alphabet_disc = \
					     self.alphabet_disc, \
					  alphabet_cont = \
					     self.alphabet_cont )
		
		mhybpow = premhyb.ComputeHybridPower()

		return mhybpow


	def HybridHankelMatrixFromTables( self, cont_length, \
					     cont_rank, \
					     disc_length,\
					     disc_size):
		repr = self.GetRepresentation()

		hanktobj     = self.automata.HankelTable(\
		                        disc_length, disc_size,\
					self.index_first)
					
		des_size  = 2*hanktobj.depth + 1
		
		stop_size = des_size * len(repr.repr_index)

		hankel_size = max([cont_length, des_size])
		hankel_rank = max([cont_rank, stop_size])
		(hankm, hsize, ind_list) = repr.HankelMatrix(\
		                                 hankel_size,\
						 hankel_rank
		                                 )
						 

		hybrid_hankel = HybridHankel(\
		                    hankel_matrix = hankm,\
				    hankel_table_obj = hanktobj,\
				    index_first = \
				         self.index_first,\
				    index_second = \
				            self.index_second, \
				    alphabet_cont = \
				             self.alphabet_cont,\
				    alphabet_disc = \
				              self.alphabet_disc,\
				    hankel_matrix_length = hsize,\
				    
				    cont_output_dimension = \
				             self.output_dimension)
		
		return hybrid_hankel
		
		
	def HybridHankelMatrix( self, cont_length, \
					     cont_rank, \
					     disc_length,\
					     disc_size):
		#repr = self.GetRepresentation()

		(hankm, hsize, ind_list) = \
		   self.GetContHankelMatrix(cont_length, cont_rank)
						 
		#automata    =  self.ComputeAutomaton(\
		#                      depth = hsize )
		automata     = self.GetExtendedAutomata()
		hanktobj     = automata.HankelTable(\
		                        disc_length, disc_size,\
					self.index_first)

		hybrid_hankel = HybridHankel(\
		                    hankel_matrix = hankm,\
				    ext_hankel_table_obj \
				    = hanktobj,\
				    index_first = \
				         self.index_first,\
				    index_second = \
				            self.index_second, \
				    alphabet_cont = \
				             self.alphabet_cont,\
				    alphabet_disc = \
				              self.alphabet_disc,\
				    hankel_matrix_length = hsize,\
				    
				    cont_output_dimension = \
				             self.output_dimension)
		
		return hybrid_hankel
