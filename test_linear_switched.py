#!/usr/bin/env python
#import form_pow_repr
from numpy import *
from sets import *
import hankel_matrix
import switched_systems
from dfa_automata import *
import sys,os
import copy



#A1=array([[0, 1 ,0, 0 ],
#            [0, 0, 0,4], [0,0,5,0],[0,0,6,1]])
#A2=array([[0,8,8,0],[0,1,0,0],[0,0,0,0],[0,7,0,1]])
#
#discrete_modes = ['q1','q2']
#a_matrices = dict([('q1',A1), ('q2',A2)])
#B1 = array([[0],
#	   [1],
#	   [0], [0]])
#B2 = array([[1],
#            [0],
#	    [0],
#	    [1]
#	   ]) 
#
#b_matrices = dict([('q1',B1),('q2',B2)])
#c_matrices = dict([('q1', array([[1, 0, 0,5]])), ('q2',array([[0, 0, -4,7]]))])
#
#initial_state = array([[0],
#                     [0],
#		     [0],
#		     [0]])
#
#initial_states=dict([(1,initial_state)])                       

if len(sys.argv) < 2:
	filename = "lin_switch_config"
else:
	filename = sys.argv[1]

if len(sys.argv) < 3:
	switched_systems.def_precision = 1e-2
else:
	switched_systems.def_precision = eval(sys.argv[2])

switched_systems.init_module(switched_systems.def_precision)
fp= open(filename)

linsys = switched_systems.LinearSwitchedSystem(config_file = fp )
     			#discrete_modes=discrete_modes, \
                        #   a_matrices=a_matrices,\
                        #   b_matrices=b_matrices, \
			#   c_matrices=c_matrices, \
			#   initial_states=initial_states)


print "Linsys \n "
linsys.ownprint ()

print "Is Obseravble \n"+ str(linsys.IsObservable())+\
      " Is Reachable \n" + str(linsys.IsReachable())+"\n"

mlinsys = linsys.MinimalSystem()

repr = linsys.ComputeRepresentation()

mrepr = repr.MinimalRepresentation()

mdim = mrepr.dimension

print "Minimal representation: \n"
mrepr.ownprint()

print "Minimal dimension :"+str(mdim)+"\n"
print "Minimal system \n"
mlinsys.ownprint()

 
print "Is Obseravble \n"+ str(mlinsys.IsObservable())+\
      " Is Reachable \n" + str(mlinsys.IsReachable())+"\n"

(hankelm1, msize1, mindex_list1) = linsys.HankelMatrix(linsys.state_dimension, mdim)

print "Hankel matrix of the original system\n"+\
       array2string(hankelm1, precision=2, suppress_small=1)+\
       " msize1: "+str(msize1)+"\n"

(hankelm2, msize2, mindex_list2) = mlinsys.HankelMatrix(msize1, \
                                             mdim)

print "Hankel matrix of the minimal system\n"+\
       array2string(hankelm2, precision=2, suppress_small=1)+"\n"
 
print "Hankel matrix difference "+\
	array2string(hankelm1-hankelm2, precision=2,\
	                                suppress_small=1)+"\n"
print "Msize1: "+str(msize1)+" index_list:"+str(mindex_list1)+"\n" 
mlinsys2 =  switched_systems.HankelMatrixToLinSwitch( hankelm1, \
                     #b_matrices[discrete_modes[0]].shape[1],
		     #c_matrices[discrete_modes[0]].shape[0],
		     linsys.input_dimension,\
		     linsys.output_dimension,\
		     linsys.discrete_modes,\
		     mindex_list1, msize1 )

print "Minimal linear switched system from the Hankel matrix\n"
mlinsys2.ownprint()

print "Hankel matrix difference:\n"
(hankelm3, msize3, mindex_list3) = mlinsys2.HankelMatrix(msize2, mdim)

print "Hankel matrix difference "+\
	array2string(hankelm1-hankelm3, precision=2,\
	                                suppress_small=1)+"\n"

#return

max_dim = linsys.state_dimension

(hank1,num1, mindex_list4) =  linsys.FirstPartialReal()

list_markov = linsys.MarkovParameterList(2*num1+1)

length_markov = 1
list_markov_long = linsys.MarkovParameterList(2*length_markov + 1)

print "Markov parameters up to length "+ str(2*num1+1)+"\n"
for (keys,value) in list_markov.iteritems():
	print str(keys) + ":" + str(value) + "\n"


print "Long list of Markov parameters up to length "+ str(2*length_markov+1)+"\n"
for (keys,value) in list_markov_long.iteritems():
	print str(keys) + ":" + str(value) + "\n"

print "hank1\n"+\
       array2string(hank1, precision=2, suppress_small=1)+"\n"

print "mindex_list4: "+str(mindex_list4)+"\n"
print "mindex_list1: "+str(mindex_list1)+"\n"
print "shape_hankm1:" + str(hankelm1.shape)+"\n"
print "shape_hankm4:" + str(hank1.shape)+"\n"


mlinsys4 =  switched_systems.HankelMatrixToLinSwitch( hank1, \
                     #b_matrices[discrete_modes[0]].shape[1],
		     #c_matrices[discrete_modes[0]].shape[0],
		     linsys.input_dimension,\
		     linsys.output_dimension,\
		     linsys.discrete_modes,\
		     mindex_list1, num1 )

print "Minimal linear switched system from the Hankel matrix\n"
mlinsys4.ownprint()

list_markov2 = mlinsys4.MarkovParameterList(2*num1+1)
print "Markov parameters of partial real. up to length "+ str(2*num1+1)+"\n"
for (keys,value) in list_markov2.iteritems():
	print str(keys) + ":" + str(value) + "\n"

print "Difference Markov parameters up to length "+ str(2*num1+1)+"\n"
for key in list_markov2.keys():
	print str(key) + ":" 
	for count in range(len(list_markov2[key])):
		(word1,value1) = list_markov[key][count]
		(word2,value2) = list_markov2[key][count]
		print "Word1:"+str(word1)+"word2:"+str(word2)+\
                      " difference: "+ str(value1-value2)+"\n"
 
print "Hankel matrix difference:\n"
(hankelm4, msize4, mindex_list4) = mlinsys4.HankelMatrix(num1, mdim)
(hankelm6, msize6, mindex_list6) = linsys.HankelMatrix(num1, mdim)


(hankelm5, msize5, mindex_list5) = mlinsys4.HankelMatrix(msize1, mdim)
print "Hankel matrix difference1 "+"\n"+\
	array2string(hankelm1-hankelm5, precision=2,suppress_small=1)+"\n"

print "Hankel matrix difference2 "+"\n"+\
	array2string(hank1-hankelm6, precision=2,suppress_small=1)+"\n"

print "Hankel matrix new 0 "+"\n"+\
	array2string(hankelm6, precision=2,suppress_small=1)+"\n"

print "Hankel matrix difference3 "+"\n"+\
	array2string(hank1-hankelm4, precision=2,suppress_small=1)+"\n"

print "Hankel matrix new "+"\n"+\
	array2string(hankelm4, precision=2,suppress_small=1)+"\n"

#return
sys.exit(-1)
 
print "Constarined switched system\n"
 
#alphabet = ['q1','q2']
#state = Set(['1','2','3'])
##output = dict([('1','o1'),('2','o2'), ('3','o1')])
#accepting_states = Set(['2'])
#transition=dict([(('q1','1'),'1'), (('q2','2'),'2'), \
#		 (('q2','1'),'2'),\
#		 (('q1','2'),'3'),\
#		 (('q1','3'),'3'), (('q2','3'),'3')])
#zeta='1'		 

fp.seek(0)
auto = LinSwitchDFA( config_file = fp)
#alphabet,state,transition,accepting_states,\
#                 zeta, linsys.input_dimension, \
#		 linsys.output_dimension, 
#		 initial_states.keys())		 

print "The automata of the constrained language\n"

auto.ownprint()

linsys_const = switched_systems.LinearSwitchedSystemConst(linswitch=linsys,\
                                          automata=auto)

print "Constrained switched system\n"
linsys_const.ownprint()					  

mlinsys_const = linsys_const.MinimalSystem()

print "Minimal constrained switched system\n"
mlinsys_const.ownprint()

repr = linsys_const.ComputeRepresentation()

mrepr = repr.MinimalRepresentation()

print "Minimal representation\n"
mrepr.ownprint()

mrank = mrepr.dimension

print "Dimension of the minimal representation: "+str(mrank)+"\n"

m_size = linsys_const.state_dimension * len(auto.state) + 1

print "M_size:\n"+str(m_size)+"\n"
#(mhank01, msiz01) = mrepr.HankelMatrix(m_size, mrank)

(chankelm1, msize1, mindex1) = linsys_const.HankelMatrix(m_size,mrank )

print "Hankel matrix of the constrained switched system\n"+\
	array2string(chankelm1, precision=2,\
	                                suppress_small=1)+\
					"size: "+str(msize1)+"\n"

(chankelm2, msize2, mindex2) = mlinsys_const.HankelMatrix(msize1, mrank)

print "Hankel matrix of the minimal constrained switched system\n"\
       + array2string(chankelm2, precision=2,\
	                                suppress_small=1)+"\n"
					

print "Hankel matrix difference \n"+\
       array2string(chankelm1 - chankelm2, precision=2, \
       suppress_small=1)+"\n"

#print "Difference between Hankel matrix of repr. and lin.sw\n"+\
#       array2string(mhank01-chankelm1,precision=2, suppress_small=1)+"\n"

print "Mindex1: "+str(mindex1)+"\n"
       
mlinsys2 =  switched_systems.HankelMatrixToLinSwitchConst( chankelm1, \
                     #b_matrices[discrete_modes[0]].shape[1],
		     #c_matrices[discrete_modes[0]].shape[0],\
		     linsys.input_dimension,\
		     linsys.output_dimension,\
		     linsys.discrete_modes,\
		     mindex1, msize1,\
		     auto )
print "Minimal constrained switched system from the Hankel-matrix\n"


mlinsys2.ownprint()

#mrepr2 = switched_systems.LinearSwitchedSystem.ComputeRepresentation(\
#               mlinsys2)
#print "Original representation\n"	       
#mrepr2.ownprint()	       
#
#(hankm4, msize4) = mrepr2.HankelMatrix(msize1, mrank)
#
#print "Hankel matrix of the representation\n"+\
#         array2string(hankm4,precision=2, suppress_small=1)+"\n"

#print "Difference between Hankel-matrix of repr. and lin.switch.const"+\
#      array2string(hankm4 - chankelm1, precision=2, \
#                    suppress_small=1)+"\n"

(chankelm3, msize3, mindex3) = mlinsys2.HankelMatrix(msize1, mrank, mindex1)



print "Hankel matrix of the minimal constrained switched system\n"\
       + array2string(chankelm3, precision=2,\
	                                suppress_small=1)+"\n"
					
print "Original? matrix\n"+ array2string(chankelm1, precision=2,\
                                          suppress_small=1)+"\n"

(chankelm11, msize11, mindex11) = linsys_const.HankelMatrix(m_size, mrank)
print "Hankel matrix difference \n"+\
       array2string(chankelm11 - chankelm3,\
       precision=2, suppress_small=1)+"\n"


 
#mlinsys3 =  switched_systems.HankelMatrixToLinSwitch( chankelm1, \
#                     #b_matrices[discrete_modes[0]].shape[1],
#		     #c_matrices[discrete_modes[0]].shape[0],\
#		     linsys.input_dimension,\
#		     linsys.output_dimension,\
#		     linsys.discrete_modes,\
#		     linsys.initial_states.keys(), msize1)
#		     
#mlinsys3_const = switched_systems.LinearSwitchedSystemConst( linswitch=mlinsys3,\
#                                            automata = auto )
#					  
#mhm6 = hankel_matrix.HankelMatrix( mrepr.alphabet, mrepr.output.shape[0],\
#                       chankelm1,\
#		       mrepr.zeta.keys(), msize1)
#
#mrepr6 = mhm6.ComputeRepresentation()
#
#(hankelm7, msize7) = mrepr6.HankelMatrix(m_size, mrank)
#
#print "Hankel difference0:\n"+\
#      array2string( hankelm7 - chankelm1, precision=2,\
#       suppress_small=1)
#
#chankelm11 = mrepr.BruteForceHankelMatrix( msize1 )
#
#mhm61 = hankel_matrix.HankelMatrix( mrepr.alphabet, mrepr.output.shape[0],\
#                       chankelm1,\
#		       mrepr.zeta.keys(), msize1)
#
#mrepr61 = mhm61.ComputeRepresentation()
#
#(hankelm71, msize71) = mrepr6.HankelMatrix(m_size, mrank)
#
#print "Hankel difference0-1:\n"+\
#      array2string( hankelm71 - chankelm11, precision=2,\
#       suppress_small=1)
# 
#hankelm72 = mrepr6.BruteForceHankelMatrix( msize1 )
#
#print "Hanke difference0-3:\n"+\
#      array2string( hankelm72 - chankelm11, precision=2,\
#                    suppress_small=1)
# 
#print "Hankel difference0-2:\n"+\
#        array2string( chankelm11 - chankelm1, precision=2,\
#	               suppress_small = 1)
#
#(hankelm5, msize5) = mlinsys3.HankelMatrix(m_size, mrank)
#
#
#(hankelm6, msize6) = mlinsys3_const.HankelMatrix(m_size, mrank)
#
#print "Hankel difference1: \n"+\
#       array2string( hankelm5 - chankelm1, precision=2,\
#                     suppress_small=1)+"\n"
#
#
#print "Hankel difference2: \n"+\
#        array2string( hankelm6 - chankelm1, precision=2,\
#	               suppress_small=1)+"\n"
