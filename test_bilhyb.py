#! /usr/bin/env python

import automata
from sets import Set
from form_pow_repr import *
from hybrid_power import *
from auxiliary_hybrid import *
from numpy import *
from bilhyb import *
import sys
import Gnuplot
from numpy.random import *
from test_bilhyb_lib import *

#def own_gnu_plot(**arguments):
#	plot_vector = []
#
#	g = arguments["plot"]
#
#	g._clear_queue()
#	row_size = arguments["outputs"]
#
#	data_list = arguments["data"]
#
#	print "Data_list"+str(len(data_list))+"\n"
#	
#	#decomp_data_list = []
#	decomp_data_list_one_row = []
#	for index in range(row_size):
#		data_key = 0
#		for vector in data_list:
#			plot_vector_element = []
#			for data_element in vector:
#				plot_vector_element.append((data_element[0],data_element[1][index]))
#			title_string = "y["+str(data_key)+"]["+str(index)+"]"
#			plot_item=Gnuplot.PlotItems.Data(plot_vector_element, with="lines", title=title_string)
#			data_key = data_key + 1
#	
#			#decomp_data_list_one_row.append(plot_item)
#			g._add_to_queue([plot_item])
#
#	print "Decomp length"+str(len(decomp_data_list_one_row))+"\n"
#	g.refresh()
#
#	return
#		
#	
#
#	
#	
#
#def general_scalar_input(time, input_sequence):
#        ctime = 0
#        uindex = 0
#        while ctime <= time and uindex < len(input_sequence):
#                ctime = input_sequence[uindex][0]
#                uindex = uindex + 1
#
#        return input_sequence[uindex-1][1]
#
#def generate_random_input_sequence(interval, step_number, mean,\
#                             covariance, input_size):
#        timeaxis = linspace(0,interval,step_number)
#        seed()
#        input_sequence=[]
#        for index in range(step_number):
#		input_value=[]
#		for jindex in range(input_size):
#                	input_value.append(normal(mean,covariance))
#               
#		input_sequence.append((timeaxis[index],input_value))
#
#        return input_sequence
#
#
#
#def test_bilhyb_system(bilinhyb, disc_input, sequence_set, markov_parameter_length=[]):
#
#	bilinhyb.ownprint()
#	
#	print "IsReachable: " + str(bilinhyb.IsReachable()) + \
#	  "IsObservable: " + str(bilinhyb.IsObservable())+"\n"
#	
#	mbilhyb = bilinhyb.ComputeMinimal()
#	
#	print "Minimal bilinear hybrid system \n"
#	
#	mbilhyb.ownprint()
#	
#	
#	
#	 
#	
#	
#	if not sequence_set == []:
#	
#		HGS1 = bilinhyb.GenerateHGS(sequence_set)
#	
#		print "HGS1 for bilinear hybrid system: " + str(HGS1)+"\n"
#	
#		HGS01 = bilinhyb.GenerateHGSBruteForce(sequence_set)
#	
#		print "HGS01 brute force for bilinear hybrid system: " + str(HGS01)+"\n"
#	
#		HGS2 = mbilhyb.GenerateHGS(sequence_set)
#	
#		print "HGS2 for minimal bilinear hybrid system: " + str(HGS2)+"\n"
#	
#		HGS02 = mbilhyb.GenerateHGSBruteForce(sequence_set)
#	
#		print "HGS02 brute force for minimal bilinear hybrid system: " + str(HGS02)+"\n"
#	
#	
#	else:
#	
#	
#		HGS1 = bilinhyb.GenerateHGSUpToLength(markov_length)
#	
#		print "HGS1 for bilinear hybrid system: " + str(HGS1)+"\n"
#	
#		HGS2 = mbilhyb.GenerateHGSUpToLength(markov_length)
#	
#		print "HGS2 for minimal bilinear hybrid system: " + str(HGS2)+"\n"	
#	
#	
#	#sys.exit(-1)
#	
#	plots=[]
#	g=Gnuplot.Gnuplot(debug=1, persist=1)
#	
#	map_counter = 0
#	
#	input_size = bilinhyb.input_size
#	output_size = bilinhyb.output_dimension
#	
#	for (key,state) in bilinhyb.zeta.iteritems():
#		input_counter = 0
#		for (switching,etime) in disc_input:
#	
#			TotalTime = etime
#	
#			for (dinput,time) in switching:
#				TotalTime = TotalTime + time 
#	
#			input_sequence = generate_random_input_sequence(\
#				TotalTime,100,0.3,1, input_size)
#	
#			input = (lambda time: general_scalar_input(\
#							time,input_sequence))
#	
#			print "Input_counter: "+str(input_counter)+\
#			" Map coounter: " + str(map_counter)+"\n"
#		
#			output_traject = bilinhyb.GenerateContOutput(\
#		 	state, switching, etime, input)
#	
#			g('set data style lines')
#			g('set hidden')
#			g('set contour base')
#			own_gnu_plot(plot=g,data=[output_traject], outputs=output_size)
#			raw_input('Please press return to continue...\n')
#			g.hardcopy('gp_plot0Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)
#	
#	        	mstate = mbilhyb.zeta[key]
#			mdstate=mstate[0]
#			mcstate=mstate[1]
#			mcstate=reshape(mcstate, (mbilhyb.state_dimensions[mdstate],1))
#			print "Mdstate: "+str(mdstate)+"state: " + \
#			str(mstate)+"\n"
#		       	moutput_traject = mbilhyb.GenerateContOutput(\
#	            	(mdstate,mcstate), switching, etime, input)
#			own_gnu_plot(plot=g,data=[moutput_traject],outputs=output_size)
#			raw_input('Please press return to continue...\n')
#			g.hardcopy('gp_plot1Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)
#		 
#	#
#			own_gnu_plot(plot=g,data=[output_traject, moutput_traject],outputs = output_size)
#			raw_input('Please press return to continue...\n') 
#			g.hardcopy('gp_plot4Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)
#	
#			input_counter = input_counter + 1
#			
#		map_counter = map_counter + 1
#	
#	return



if len(sys.argv) < 2:
        filename = "test_linhyb_config"
else:
        filename = sys.argv[1]

fp = open(filename)


bilinhyb = BilinearHybridSystem(config_file=fp)

fp.seek(0)

configf = ConfigParser.ConfigParser()
configf.readfp(fp)

if configf.has_option("Simulation","input"):
	input = eval(configf.get("Simulation",\
	              "input"))
else:
	input = ( lambda time:  1 )


if configf.has_option("Simulation","disc_input"):
	disc_input = eval(configf.get("Simulation",\
                         "disc_input"))
			 	
else:
	disc_input = [ ([('b',2.9),('a',1.8),('a',2.7),('b',1.9)], 2.9)]

if configf.has_option("Simulation", "HGS_Word_List"): 
	sequence_set = eval(configf.get("Simulation", "HGS_Word_List"))
	markov_length = 0
else:
	sequence_set = []
	if configf.has_option("Simulation", "MarkovParameterLength"):
		markov_length = eval(configf.get("Simulation", "MarkovParameterLength"))
	else:
		markov_length = 3


test_bilhyb_system(bilinhyb, disc_input, sequence_set, markov_length)
