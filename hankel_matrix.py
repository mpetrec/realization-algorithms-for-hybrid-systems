from form_pow_repr import *
from word_operations import *
from numpy import *
from numpy.linalg import *
import copy

from own_io import own_print

is_output=False
#is_output = True

def_precision = 1e-10

#def IndexToNumber( alphabet, index_set, word, index ):
#        number = 0
#	d = len(alphabet)
#	for letter in word:
#		number=d*(number)+ word[1]
#	return number+index_set[index]	
#
#def SetToDict( input_set ):
#	index = 0
#	dict_input_set = []
# 	for element in input_state 
#		dict_input_set.append((element, index))
#		index = index + 1
#	return dict_input_set	
#	
#def WordGenerate( alphabet, depth ):
#	if depth == 0:
#		return []
#	
#	swordlist = WordGenerate(alphabet, depth-1)
#
#	wordlist = []
#	for letter in alphabet:
#		wordlist.append([letter])
#		
#	for word in wordlist:
#		for letter in alphabet:
#			word.append(letter)
#			wordlist.append(word)
#	
#	return wordlist
	
class HankelMatrix:
	def __init__(self,alphabet,odimension,matrix,index_set,length ):
 		self.dict_alphabet = SetToDict( alphabet )
		self.length = length
		self.alphabet = alphabet
		self.odimension = odimension 
		self.dict_odimension = SetToDict( range( odimension )) 
		self.matrix = matrix
		self.index = index_set
		self.dict_index = SetToDict( index_set )

	def ownprint(self):
		print "Dict_alphabet:"+str(self.dict_alphabet)+"\n"
		print "Lenght: "+str(self.length)+"\n"
		print "Alphabet: "+str(self.alphabet)+"\n"
		print "Odimension:"+str(self.odimension)+"\n"
		print "Dict_Odimension"+str(self.dict_odimension)+"\n"
		print "Matrix: "+str(self.matrix)+"\n"
		print "Index:"+str(self.index)+"\n"
		print "Dict_index:"+str(self.dict_index)+"\n"
		
	def GetColumn(self, word, index,matrix=[]):
		if matrix==[]:
			matrix=self.matrix
		number=IndexToNumber(
		  self.dict_alphabet, self.dict_index, word,index)
		return reshape(matrix[:,number],(matrix.shape[0],1))
	
	def GetRow(self, word, index, matrix=[]):
		if matrix==[]:
			matrix=self.matrix
		number=IndexToNumber(
		  self.dict_alphabet, self.dict_odimension, word,index)
		own_print(is_output,"RowNumber:"+str(number)+"\n"  )
		return reshape(matrix[number,:], (1,matrix.shape[1]))
	
	def GetElement(self, rword, rindex, cword, cindex):
		rnumber=IndexToNumber(
		  self.dict_alphabet, self.dict_odimension, rword, rindex)
		cnumber=IndexToNumber(
		  self.dict_alphabet, self.dict_index, cword,cindex)

		return self.matrix[rnumber,cnumber]

	def ComputeRH(self, matrix, letter):
		wordset1 = WordGenerate(self.dict_alphabet, self.length)	
		is_empty = True
 		for word in wordset1:
			lword = copy.deepcopy(word)
			lword.insert(0,letter)
			if lword == []:
				own_print(is_output,"MISTAKE\n")
			for i in self.dict_odimension:	
				if is_empty:
					RH = self.GetRow(lword,i,matrix)
					is_empty = False
				else:
					RH = concatenate((RH, self.GetRow(lword,i,matrix)),0)
					
		return RH
		
	def ComputeLH(self, matrix):	
		wordset = WordGenerate(self.dict_alphabet,self.length)

		is_empty=True
		for word in wordset:
			for i in self.dict_odimension:
				if is_empty:
					LH=self.GetRow(word,i,matrix)
					is_empty=False
				else:
					LH=concatenate((LH, self.GetRow(word,i,matrix)),0)
				own_print(is_output,"WordLH:"+str(word)+"LH: "+str(LH)+"\n"		)
		return LH

	def GetSingularValues(self):
			
		svd_decomp = compute_own_svd(self.matrix, \
		                             cutoff = new_def_precision)
		#is_output = False
		
		if svd_decomp == "No solution":
			print "No solution to the decomp\n"
			S = array([[0]])
			U = zeros((self.matrix.shape[0],1))
			VT = zeros((1,self.matrix.shape[1]))
		else:		
			U=svd_decomp[0]
			S=svd_decomp[1]
			VT=svd_decomp[2]
	
		own_print(True, "SVD:"+str(S)+"\n")
		
		return S

		
	def ComputeRepresentation(self, new_def_precision=def_precision):
		is_output = True
		svd_decomp = compute_own_svd(self.matrix, \
		                             cutoff = new_def_precision)
		#is_output = False
		
		own_print(is_output,\
		   "Def_precision :"+str(def_precision)+"\n")    

		if svd_decomp == "No solution":
			print "No solution to the decomp\n"
			S = array([[0]])
			U = zeros((self.matrix.shape[0],1))
			VT = zeros((1,self.matrix.shape[1]))
		else:		
			U=svd_decomp[0]
			S=svd_decomp[1]
			VT=svd_decomp[2]
	
		own_print(True, "SVD:"+array2string(S)+"\n")
		
		O = matrixmultiply(U, sqrt(S))
		R = matrixmultiply(sqrt(S),VT)
		own_print(is_output,"O:"+str(O)+"R: "+str(R)+\
		 "OR:" +str(matrixmultiply(O,R))+"\n")
	
		NEW_MATRIX = matrixmultiply(O,R)

		(NEW_U,NEW_S,NEW_V) = compute_own_svd(NEW_MATRIX)

		own_print(True, "SVD_NEW:"+array2string(NEW_S)+"\n")
		
		
		is_empty = True
		for i in self.dict_odimension:
			if is_empty:
				output=self.GetRow([],i,O)
				is_empty=False
			else:	
				output = concatenate((output, self.GetRow([],i,O)),0)
		
        	zeta = dict()
		for i in self.dict_index:
			zeta[i[0]] = self.GetColumn([],i,R)

   		transition = dict()	
		LH = self.ComputeLH(O)
		(rank,garbage) = compute_rank(LH)

		own_print(True, "LH rank:"+str(rank)+"\n")
		for letter in self.dict_alphabet:
			own_print(is_output,"LH:"+str(LH)+"\n")
			RH = self.ComputeRH( O, letter )
			
			own_print(is_output,"RH: "+str(RH)+"\n")
			own_print(is_output,"Letter:"+str(letter[0])+"\n")

			retval = linear_least_squares(LH, RH,\
			             def_precision)
			own_print(is_output,\
			"Retval0 :"+array2string(retval[0], \
			suppress_small=1)+\
			" Cost function: "+array2string(\
			   retval[1], suppress_small=1)+\
			 "Rank of LH:"+str(retval[2])+\
			 "Singular values of LH:"+\
			 array2string(retval[3], suppress_small=1)+\
                         "\n" + " difference:"+\
                         array2string((RH-matrixmultiply(LH,retval[0])), suppress_small=1)+\
			 "\n")
			transition[letter[0]] = retval[0]
			own_print(is_output,"Trans:"+str((matrixmultiply(LH,retval[0])-RH))+"\n")

		repr = Representation(self.alphabet, transition, output, zeta)

		return repr
		
