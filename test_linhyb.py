#! /usr/bin/env python

import automata
from sets import Set
from form_pow_repr import *
from hybrid_power import *
from auxiliary_hybrid import *
from numpy import *
from numpy.random import *
from linhyb import *
import sys
import Gnuplot

def general_scalar_input(time, input_sequence):
	ctime = 0
	uindex = 0
	while ctime <= time and uindex < len(input_sequence):
		ctime = input_sequence[uindex][0]
		uindex = uindex + 1
	
	return input_sequence[uindex-1][1]

def generate_random_input_sequence(interval, step_number, mean,\
                             covariance):
	timeaxis = linspace(0,interval,step_number)
	seed()
	input_sequence=[]
	for index in range(step_number):
		input_value=normal(mean,covariance)
		input_sequence.append((timeaxis[index],input_value))

	return input_sequence	
		
#import fpectl


#fpectl.turnon_sigfpe()

#alphabet = (['a','b'])
#state = Set(['1','2'])
#output = dict([('1','o'),('2','o')])
#transition=dict([(('a','1'),'1'), (('a','2'),'1'), \
#		 (('b','1'),'2'), (('b','2'),'2')])
#		 #(('a','3'),'2'), (('b','3'),'3')])
#zeta=dict([(1,'1')])		 
#auto = automata.MooreAutomata(alphabet,state,transition,output,zeta)		 
#auto.ownprint()
#
#A1 = array([\
#          [1,0,0],\
#	  [0,3,0],\
#	  [0,0,4]])
#	  
#B1 = array([[1],[0],[0]])
#C1 = array([[1,1,1]])
#A2 = array([[2, 0],  \
#	      [0,1]])
#B2 = array([[0],[1]])
#C2 = array([[1,1]])
# 
# #The linear reset maps are the following
#Ma2 = array([[0,1], [1, 0], [0,0]])
#Mb1 = array([[0, 1, 0], [1, 0,0]])
#Ma1 = array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
#Mb2=array([[1,0],[0,1]])
#
#initial_state = ('2', array([[1],[0]]))
#
#zeta = dict([(1,initial_state)])
#
#alphabet_disc = alphabet 
#alphabet_cont = ['e']
#
#index_first = [1]
#
#index_second = [1]
#
#a_matrices = dict([('1',A1),('2',A2)])
#
#b_matrices = dict([('1', B1),\
#                   ('2', B2)])
#
#c_matrices = dict([('1',C1), ('2',C2)])
#
#reset_maps = dict([(('a','1'),Ma1),\
#                     (('a','2'),Ma2), \
#		     (('b','1'), Mb1), \
#		     (('b','2'), Mb2)])
		     

#hybpow = HybridPower( automata = auto, \
#                      alphabet_disc = alphabet_disc,\
#		      alphabet_cont = alphabet_cont, \
#		      index_first = index_first, \
#		      index_second = index_second, \
#		      a_matrices = a_matrices,\
#		      b_matrices = b_matrices, \
#		      c_matrices = c_matrices,\
#		      reset_maps = reset_maps,
#		      zeta = zeta )

if len(sys.argv) < 2:
	filename = "test_linhyb_config"
else:
	filename = sys.argv[1]

fp = open(filename)

linhyb = LinearHybridSystem( config_file = fp )
#			    automata = auto, \
#                            alphabet = alphabet, \
#			    index = index_first,\
#			    a_matrices = a_matrices, \
#			    b_matrices = b_matrices, \
#			    c_matrices = c_matrices,\
#			    reset_maps = reset_maps, \
#			    zeta = zeta )

linhyb.ownprint()

print "Representation\n"

repr = linhyb.GetRepresentation()

repr.ownprint()

mlinhyb = linhyb.ComputeMinimal()

print "Minimal linear hybrid system \n"

mlinhyb.ownprint()

cont_size = []
#mlinhyb.GetContStateSize()
disc_size = mlinhyb.GetDiscreteStateSize()
print "Compute Hankel matrices of size\n"
print "Disc. size:"+str(disc_size)+\
      " \n Cont. size:"+str(cont_size)+"\n"

lin_hyb_hank1 = linhyb.HybridHankelMatrix( cont_size, \
                        cont_size, disc_size, disc_size )

print "Original hankel matrix\n"

lin_hyb_hank1.ownprint()

lin_hyb_hank2 = mlinhyb.HybridHankelMatrix( cont_size, \
                       cont_size, disc_size, disc_size )
		       
print "Hankel matrix of the minimal system\n"
lin_hyb_hank2.ownprint()

print "Hankel matrix difference\n"
print str(lin_hyb_hank2 == lin_hyb_hank1)+"\n"

mh_linhyb = lin_hyb_hank1.ComputeLinearHybridSystem()

print "Linear hybrid system from the hankel matrix\n"

mh_linhyb.ownprint()

lin_hyb_hank3 = mh_linhyb.HybridHankelMatrix( cont_size, \
                         cont_size, disc_size, disc_size )

print "Hankel matrix of the linear hybrid system computed \
        from the Hankel-matrix\n"

lin_hyb_hank3.ownprint()

print "Hankel matrix difference\n"
print str(lin_hyb_hank3 == lin_hyb_hank1)+"\n"
	
#hybpow.ownprint()
  			
#repr = hybpow.ComputeRepresentation()  

#print "Representation:\n"
#repr.ownprint()  

#mrepr = repr.MinimalRepresentation()

#print "Minimal representation \n"
#mrepr.ownprint()

#auto = hybpow.ComputeAutomaton(repr = repr, depth = 1 )

#auto.ownprint()
 
#mauto = auto.MinimalAutomaton()

#print "Minimal automaton \n"
#mauto.ownprint()


#phybpow = PreHybridPower( repr = mrepr, automata = mauto, \
#		      index_first = index_first, \
#                      alphabet_disc = alphabet_disc,\
#		      alphabet_cont = alphabet_cont, \
#		      index_second = index_second)

#mhybpow = phybpow.ComputeHybridPower()

#print "Minimal hybrid representation\n"

#mhybpow.ownprint()

print "Original system: \n"

is_reach = linhyb.IsReachable()
print "IsReachable: " + str(is_reach) + "\n"

is_obsv = linhyb.IsObservable()

print "IsObservable: " + str(is_obsv)+"\n"

print "End Output\n"

fp.seek(0)

configf = ConfigParser.ConfigParser()
configf.readfp(fp)

if configf.has_option("Simulation","input"):
	input = eval(configf.get("Simulation",\
	              "input"))
else:
	input = ( lambda time:  1 )


if configf.has_option("Simulation","disc_input"):
	disc_input = eval(configf.get("Simulation",\
                         "disc_input"))
			 	
else:
	disc_input = [ ([('b',2.9),('a',1.8),('a',2.7),('b',1.9)], 2.9)]
 
plots=[]
g=Gnuplot.Gnuplot(debug=1)

map_counter = 0
for (key,state) in linhyb.zeta.iteritems():
        #dstate = states[0]
	#cstate = states[1]
	#constate_size = linhyb.state_dimensions[dstate]
	#cstate = reshap(cstate,(constate_size,1))
#	derivative=linhyb.RightHandSide(0,(dstate,zero_init), input)
	#print "Derivative: " + array2string(derivative) + "\n"

	#state_traject = linhyb.GenerateStateTrajectory(\
	#state, switching, etime, input)

#	g.plot(state_traject)
#	raw_input('Please press return to continue...\n')
#        g.reset()
	input_counter = 0
	for (switching,etime) in disc_input:

		TotalTime = etime

		for (dinput,time) in switching:
			TotalTime = TotalTime + time 

		input_sequence = generate_random_input_sequence(\
			TotalTime,100,3,1)

		input = (lambda time: general_scalar_input(\
						time,input_sequence))

		print "Input_counter: "+str(input_counter)+\
		" Map coounter: " + str(map_counter)+"\n"
	
		output_traject = linhyb.GenerateContOutput(\
	 	state, switching, etime, input)

		g('set data style lines')
		g('set hidden')
		g('set contour base')
		g.plot(output_traject)
		raw_input('Please press return to continue...\n')
		g.hardcopy('gp_plot0Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)

        	mstate = mh_linhyb.zeta[key]
		mdstate=mstate[0]
		mcstate=mstate[1]
		mcstate=reshape(mcstate, (mh_linhyb.state_dimensions[mdstate],1))
		print "Mdstate: "+str(mdstate)+"state: " + \
		str(mstate)+"\n"
	       	moutput_traject = mh_linhyb.GenerateContOutput(\
            	(mdstate,mcstate), switching, etime, input)
		g.plot(moutput_traject)
		raw_input('Please press return to continue...\n')
		#g.hardcopy('gp_plot1.ps', enhanced=1, color=1)
		g.hardcopy('gp_plot1Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)
	 
	        msstate = mlinhyb.zeta[key]
		msoutput =mlinhyb.GenerateContOutput(\
		 msstate, switching, etime, input)
		g.plot(msoutput)
#		raw_input('Please press return to continue...\n')
		#g.hardcopy('gp_plot2.ps', enhanced=1, color=1)
		g.hardcopy('gp_plot2Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)
		
		g.plot(output_traject, msoutput,) 
#		raw_input('Please press return to continue...\n') 
		#g.hardcopy('gp_plot3.ps', enhanced=1, color=1)
		g.hardcopy('gp_plot3Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)

		g.plot(output_traject, moutput_traject,) 
#		raw_input('Please press return to continue...\n') 
		#g.hardcopy('gp_plot4.ps', enhanced=1, color=1)
		g.hardcopy('gp_plot4Input'+str(input_counter)+'Map'+str(map_counter)+'.ps', enhanced=1, color=1)

		input_counter = input_counter + 1
		
	map_counter = map_counter + 1
	
#mhybpow = hybpow.ComputeMinimal()
#print "Minimal system\n"
#
#mhybpow.ownprint()
#print "IsReachable: " + str(mhybpow.IsReachable()) + \
#      "IsObservable: " + str(mhybpow.IsObservable())+"\n"
