from hybrid_power import *
from scipy import integrate



class BilinearHybridSystem(HybridPower):
	Input_attribute_list = [ "index", "automata", "alphabet","reset_maps",\
	                         "a_matrices", "b_matrices",\
				 "c_matrices", "zeta"]


        Config_file_attrib  = [ "index", "alphabet",\
                                 "b_matrices", "a_matrices",\
                                 "c_matrices", "zeta", "reset_maps"]
	
	
	def __init__(self, **arguments):
		if arguments.has_key("hyb_pow"):
			HybridPower.__init__(self, **arguments)
			self.__dict__["input_size"]=len(self.alphabet_cont)-1
			return
			

		input_data = dict()
		if arguments.has_key("config_file"):
                        fp      = arguments["config_file"]

                        configf = ConfigParser.ConfigParser()

                        configf.readfp(fp)

                        for field in \
                          BilinearHybridSystem.Config_file_attrib:
                                print "Processing field:"+\
                                      str(field)+"\n"
                                input_data[field] = eval(\
                                     configf.get(\
                                       "BilinearHybridSystem",field)\
                                   )
                        fp.seek(0)
			input_data["automata_config_file"] = fp
		else:
			for field in BilinearHybridSystem.Input_attribute_list:
				input_data[field]=arguments[field]


		input_data["index_first"] = input_data["index"]
		input_data["alphabet_disc"] = input_data[ \
		                                "alphabet"]

		a_matrices = dict()
		for (key,value) in input_data[\
		              "a_matrices"].iteritems():
			a_matrices[(key,0)] = value 
		

		alphabet_cont = [ 0 ]

		input_size = 0
		
		for (key, value) in input_data[\
		                      "b_matrices"].iteritems():
				      
			if not key[1] in alphabet_cont:
				alphabet_cont.append(key[1])
			if not key[1] <= input_size:
				input_size  = input_size + 1
			a_matrices[key] = input_data["b_matrices"\
			                               ][key]	
			
		input_data["a_matrices"] = a_matrices
		input_data["b_matrices"] = dict()
		input_data["alphabet_cont"] = alphabet_cont
		input_data["index_second"] = []
		self.__dict__["input_size"] = input_size 

		print "Input size: " + str(input_size)+"\n"

		HybridPower.__init__ (self, **input_data)
		
	def ComputeMinimal(self):
		mhybpow = HybridPower.ComputeMinimal(self)
		mbilhyb = BilinearHybridSystem( hyb_pow = mhybpow )

		return mbilhyb


	def RightHandSide(self, time, current_state, input):
	        discrete_state = current_state[0]
		cont_state     = current_state[1]
		a_matrix = self.a_matrices[(discrete_state,0)] #.astype('d')
		b_matrix_list = []
		input_size = self.input_size
		cont_state_size = self.state_dimensions[discrete_state]

		cont_state = reshape(cont_state,(cont_state_size,1))#.astype('d')
	        derivative = matrixmultiply(a_matrix, cont_state)
		#b_matrix = zeros((cont_state_size,input_size), 'd')
		#for i in range(cont_state_size):
		for j in range(input_size):
			derivative = derivative +\
                                matrixmultiply(\
		     	        self.a_matrices[(discrete_state,j+1)],cont_state)*\
                                input(time)[j]
             

		#print "Time: " + str(time)+ "input: "+\
		#  str(input(time))+"\n"
		#print "Derivative: "+str(derivative)+"\n"   
		return derivative
		#return reshape(derivative,(cont_state_size,))   
			
	def GenerateStateTrajectory(self, state, \
	                         switching, etime, cont_input):
		gtime = 0
		#step_number = 1000
		step_size=0.01
		atol = 1e-8
		rtol = 1e-8
		nsteps = 1000
		method='adams'
		d_state = state[0]
		c_state = reshape(state[1], (self.state_dimensions[d_state],1))#.astype('d')
		traject = []
		for (input,time) in switching:
			def rhside(stime,istate):
				print "Init: "+str(istate)+"\n"
				return self.RightHandSide(stime, (d_state, istate), cont_input)
				
			solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
			traject.append([gtime, (d_state,c_state)])
			print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
			while solver.t  < time + gtime and \
			 solver.successful():
			 	c_state = solver.integrate(solver.t+step_size)
				c_state = reshape(c_state, (self.state_dimensions[d_state],1))#.astype('d')
				traject.append([solver.t,(d_state,c_state)])
					
			c_state = matrixmultiply(self.reset_maps[(input,d_state)], c_state) #.astype('d'))
			d_state =  \
			  self.automata.transition[(input, d_state)]
			
			gtime = gtime + time
			  
		print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
		def rhside(stime,istate):
			#print "Init:"+str(istate)+"stime: "+str(stime)+"type: "+str(istate.typecode())+"\n"
			return self.RightHandSide(stime, (d_state, istate), cont_input)
			
		solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
		traject.append([gtime,(d_state,c_state)])
		while solver.t  < etime + gtime and \
		solver.successful():
			#print "cstate:"+ array2string(c_state) + " discrete state" +str(d_state)+ "\n"
	 		c_state = solver.integrate(solver.t+step_size)
			c_state = reshape(c_state, \
			  (self.state_dimensions[d_state],1))#.astype('d')
			traject.append([solver.t,(d_state,c_state)])

		#stime = gtime
		#time_axis = [gtime]
		#while stime < gtime + etime:
		#	stime = stime+ step_size
		#	time_axis.append(stime)
				
#		 	#	c_state = solver.integrate(solver.t+step_size)
		#atime_axis=reshape(array(time_axis, 'd'),len(time_axis))	
		#c_state=reshape(c_state, self.state_dimensions[d_state])
		#atime_axis=linspace(gtime, gtime+etime,step_number)
		#solution = integrate.odeint(rhside,c_state, atime_axis)
		#for rindex in range(atime_axis.shape[0]):
		#   	stime = atime_axis[rindex]
		#	c_state = transpose(solution[0][rindex,:])
		#	traject.append([stime, (d_state,c_state)])
		
	     	
		return traject

	def GenerateStateTrajectoryWithGuards(self, state, \
	                         guard_function, etime, cont_input):
		gtime = 0
		#step_number = 1000
		step_size=0.01
		atol = 1e-8
		rtol = 1e-8
		nsteps = 1000
		method='adams'
		d_state = state[0]
		c_state = reshape(state[1], (self.state_dimensions[d_state],1))#.astype('d')
		traject = []
		for (input,time) in switching:
			def rhside(stime,istate):
				print "Init: "+str(istate)+"\n"
				return self.RightHandSide(stime, (d_state, istate), cont_input)
				
			solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
			traject.append([gtime, (d_state,c_state)])
			print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
			while solver.t  < time + gtime and \
			 solver.successful():
			 	c_state = solver.integrate(solver.t+step_size)
				c_state = reshape(c_state, (self.state_dimensions[d_state],1))#.astype('d')
				traject.append([solver.t,(d_state,c_state)])
					
			c_state = matrixmultiply(self.reset_maps[(input,d_state)], c_state) #.astype('d'))
			d_state =  \
			  self.automata.transition[(input, d_state)]
			
			gtime = gtime + time
			  
		print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
		def rhside(stime,istate):
			#print "Init:"+str(istate)+"stime: "+str(stime)+"type: "+str(istate.typecode())+"\n"
			return self.RightHandSide(stime, (d_state, istate), cont_input)
			
		solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
		traject.append([gtime,(d_state,c_state)])
		while solver.t  < etime + gtime and \
		solver.successful():
			#print "cstate:"+ array2string(c_state) + " discrete state" +str(d_state)+ "\n"
	 		c_state = solver.integrate(solver.t+step_size)
			c_state = reshape(c_state, \
			  (self.state_dimensions[d_state],1))#.astype('d')
			traject.append([solver.t,(d_state,c_state)])

		#stime = gtime
		#time_axis = [gtime]
		#while stime < gtime + etime:
		#	stime = stime+ step_size
		#	time_axis.append(stime)
				
#		 	#	c_state = solver.integrate(solver.t+step_size)
		#atime_axis=reshape(array(time_axis, 'd'),len(time_axis))	
		#c_state=reshape(c_state, self.state_dimensions[d_state])
		#atime_axis=linspace(gtime, gtime+etime,step_number)
		#solution = integrate.odeint(rhside,c_state, atime_axis)
		#for rindex in range(atime_axis.shape[0]):
		#   	stime = atime_axis[rindex]
		#	c_state = transpose(solution[0][rindex,:])
		#	traject.append([stime, (d_state,c_state)])
		
	     	
		return traject

	def GenerateContOutput(self, state, etime, \
	      switching, input ):
	      	state_traject = self.GenerateStateTrajectory(\
		          state, etime, switching, input)
		output_traject = []	  
		for traject_point in state_traject:
		        time = traject_point[0]
			dstate = traject_point[1][0]
			cstate = traject_point[1][1]
			output = matrixmultiply(self.c_matrices[dstate], cstate)
			output_traject.append([time,output])
		
		return output_traject


	def GenerateHGS(self, sequence_set):
	  	mrepr = self.GetMinimalRepresentation()
		
		HGS = []
		for word in sequence_set:
			for index in self.index_first:
				hgs_list = []
				for oindex in range(self.output_dimension):
					hgs_element = mrepr.ComputeMarkovParameter(word,index,oindex)
					hgs_list.append(hgs_element)
			HGS.append((word,index,hgs_list))

		return HGS


	def GenerateOneHGSBrute(self,word,index):
		(hgs_disc,hgs_cont) = self.zeta[index]

		for letter in word:
			if letter in self.alphabet_cont:
				hgs_cont = matrixmultiply(self.a_matrices[(hgs_disc,letter)],hgs_cont)
			elif letter in self.alphabet_disc:
				hgs_cont = matrixmultiply(self.reset_maps[(letter, hgs_disc)],hgs_cont)
				hgs_disc = self.automata.transition[(letter,hgs_disc)]

		
	
		output_hgs = matrixmultiply(self.c_matrices[hgs_disc],hgs_cont)

		return output_hgs		

	def GenerateHGSBruteForce(self, sequence_set):
		HGS = []
		for word in sequence_set:
			for index in self.index_first:
				HGS.append((word,index, self.GenerateOneHGSBrute(word,index)))

		return HGS

	
	def GenerateHGSUpToLength(self, length):

		mrepr = self.GetMinimalRepresentation()

		alphabet = mrepr.alphabet
		sequence_set = WordGenerate(alphabet, length)

		return self.GenerateHGS(sequence_set)

